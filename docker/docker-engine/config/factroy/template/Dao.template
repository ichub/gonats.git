package dao


/*
   @Title  文件名称: {{.FileName}}
   @Description 描述: {{.Description}}

   @Author  作者: {{.Author}}  时间({{.DATETIME}})
   @Update  作者: {{.Author}}  时间({{.DATETIME}})

*/


import (
    "fmt"

	"ichubengine-server/common/base/basedto"
 	"ichubengine-server/common/base/basedao"
     "strconv"
    "log"
    "strings"
    "github.com/jinzhu/gorm"
 	"ichubengine-server/common/base/basemodel"

	"ichubengine-server/common/base/pagedto"
	"ichubengine-server/common/base/baseconsts"
    "ichubengine-server/common/dbcontent"
    "ichubengine-server/shop/dto"
    "ichubengine-server/shop/model"
    "time"
)

var Inst{{.ModelName}}DAO  {{.ModelName}}DAO



type {{.ModelName}}DAO struct {
    basedao.BaseDao
}

func (this *{{.ModelName}}DAO) FindData(result *basedto.IchubResult) *model.{{.ModelName}} {
	return result.Data.(*model.{{.ModelName}})
}

func (this *{{.ModelName}}DAO) FindDataList(result *pagedto.IchubPageResult) *[]model.{{.ModelName}} {
	return result.Data.(*[]model.{{.ModelName}})
}

/*
   @title     函数名称:  Insert
   @description      :  新增记录
   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: entity *model.{{.ModelName}}
   @return    返回参数名: {{.pkeyType}},error
*/
func ( this * {{.ModelName}}DAO) Insert(entity *model.{{.ModelName}}) ({{.pkeyType}},error) {
	 
	err := this.GetDB().Create(entity).Error
    if err != nil {
	    log.Println(err.Error())
	    return -1, err
	}

	return   entity.{{.pkey}}, err
} 

/*
   @title     函数名称:  DeleteById
   @description      :  根据主键{{.pkey}} 删除记录

   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: {{.pkey}} {{.pkeyType}}
   @return    返回参数名: error
*/
func ( this * {{.ModelName}}DAO) DeleteById({{.pkey}} {{.pkeyType}}) error {
	var entity model.{{.ModelName}}

	err := this.GetDB().Where("{{.pkeyField}}=?", {{.pkey}}).Delete(&entity).Error
	return err
}
 	
	
/*

   @title     函数名称:  Save
   @description      :  保存记录

   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: entity *model.{{.ModelName}}
   @return    返回参数名: {{.pkeyType}},error
*/
func ( this * {{.ModelName}}DAO) Save(entity *model.{{.ModelName}}) ({{.pkeyType}},error)  {

	err := this.GetDB().Save(entity).Error
    if err != nil {
	    log.Println(err.Error())
	    return -1, err
	}
	return  entity.{{.pkey}}, err
}


/*

   @title     函数名称:  Update
   @description      :  修改记录

   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: entity *model.{{.ModelName}}
   @return    返回参数名: {{.pkeyType}},error
*/
func ( this * {{.ModelName}}DAO ) Update(entity *model.{{.ModelName}}) ({{.pkeyType}},error) {

	err := this.GetDB().Model(&model.{{.ModelName}}{}).Where("{{.pkeyField}}=?", entity.{{.pkey}}).Updates(entity).Error
    if err != nil {
	    log.Println(err.Error())
	    return -1, err
	}
	return   entity.{{.pkey}}, err
}


 func ( this * {{.ModelName}}DAO ) UpdateNotNull({{.pkey}} {{.pkeyType}},maps map[string]interface{}) ({{.pkeyType}},error) {

		err := this.GetDB().Model(&model.{{.ModelName}}{}).Where("{{.pkeyField}}=?", {{.pkey}}).Updates(maps).Error
		return Id, err
}

/*
   @title     函数名称:  UpdateMap
   @description      :  根据主键{{.pkey}},map修改指定字段

   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: Id int64,entity map[string]interface{}
   @return    返回参数名: int64,error
*/
func ( this * {{.ModelName}}DAO ) UpdateMap({{.pkey}} {{.pkeyType}},maps map[string]interface{}) ({{.pkeyType}},error) {

		err := this.GetDB().Model(&model.{{.ModelName}}{}).Where("{{.pkeyField}}=?", {{.pkey}}).Updates(maps).Error
		return Id, err
}
/*
   @title     函数名称:  FindById
   @description      :  根据主键{{.pkey}} 查询记录

   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: {{.pkey}} {{.pkeyType}}
   @return    返回参数名: entity *model.{{.ModelName}}, found bool, err error
*/
func ( this * {{.ModelName}}DAO) FindById({{.pkey}} {{.pkeyType}})   ( entity *model.{{.ModelName}}, found bool, err error )   {

    entity = new(model.{{.ModelName}})

	db := this.GetDB().First(entity, {{.pkey}})
	found = !db.RecordNotFound()
    err   = db.Error

	return entity, found, err

}


/*
   @title     函数名称:  FindByIds
   @description      :  根据主键{{.pkey}} 查询多条记录; FindByIds("1,36,39")

   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: pks string
   @return    返回参数名: *[]model.{{.ModelName}},error
*/
func ( this * {{.ModelName}}DAO) FindByIds(pks string) (*[]model.{{.ModelName}}, error) {

	var entities []model.{{.ModelName}}
	db := this.GetDB().Raw("select * from {{.TableName}} where {{.pkeyField}} in ("+ pks +")").Scan(&entities)

	return &entities, db.Error
}


/*
   @title     函数名称:  Query
   @description      :  查询符合条件的记录！通用条件分页

   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: param *pagedto.IchubPageRequest
   @return    返回参数名: *pagedto.IchubPageResult
*/
func ( this * {{.ModelName}}DAO) Query(param *pagedto.IchubPageRequest) (*pagedto.IchubPageResult) {

	var entities []model.{{.ModelName}}
	return param.Query(&model.{{.ModelName}}{}, &entities)
}


/*
   @title     函数名称:  Count
   @description      :  计算符合条件的记录总数！IchubPageRequest通用条件

   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: param *pagedto.IchubPageRequest
   @return    返回参数名: int, error
*/
func ( this * {{.ModelName}}DAO) Count(param *pagedto.IchubPageRequest)  ( int, error )  {

	return param.Count(&model.{{.ModelName}}{})
}
