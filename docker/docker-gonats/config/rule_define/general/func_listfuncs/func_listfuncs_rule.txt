rule "func-listfuncs-rule" "func-listfuncs-rule" salience 0
begin


    Out.SetParam("FuncId","func-listfuncs")
    funs = RuleFunc.ListFuncs()

    Out.SetReturn(200,"计算成功!")
    Out.SetParam("funs",funs)
end