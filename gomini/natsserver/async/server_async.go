package main

import (
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/goperfstat"

	"gitee.com/ichub/gonats/gomini/natsserver/async/serverasync"
	"time"
)

func main() {
	goutils.Info("starting serverAsync....")

	goperfstat.FindBeanGoperfStat().StartStats()
	var serverAsync = serverasync.FindBeanServerAsync()
	conn, _ := serverAsync.StartServer()
	if conn == nil {
		panic("start serverAsync error")
		return
	}
	defer conn.Close()
	for {
		time.Sleep(time.Second * 60 * 100)
	}
	serverAsync.CloseChannels()
}
