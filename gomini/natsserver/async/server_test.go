package main

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"

	"gitee.com/ichub/gonats/gomini/natsserver/async/cliasync"
	"github.com/gogf/gf/util/gconv"
	"testing"
)

func Test001_asyncCli_publish(t *testing.T) {

	for i := 0; i < 100; i++ {

		var msg = gomsg.FindBeanGonatsMsg()
		msg.Header.ObjectId = gconv.String(baseutils.SnowflakeNextVal())

		msg.Header.Topic = goconsts.Topic_DOMAIN_GENEAL_AsyncES_BIZ
		var result = basedto.NewIchubResult()
		msg.SetData(result)
		msg.Header.MsgReq = true
		msg.Parse2Result()

		var err = cliasync.FindBeanCliAsync().Publish(msg)
		if err != nil {
			goutils.Error(err)
		}
		goutils.Info(msg.ToPrettyString())
	}

}
