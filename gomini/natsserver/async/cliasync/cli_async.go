package cliasync

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/gonats/goproxy"

	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
)

// CliAsync 是一个实现异步客户端功能的结构体，
// 继承自 goproxy.GonatsProxy 以使用 NATS 连接功能，
// 并集成 basedto.BaseEntitySingle 以使用基础实体单例功
type CliAsync struct {
	*goproxy.GonatsProxy
	basedto.BaseEntitySingle
}

func NewCliAsync() *CliAsync {
	var c = &CliAsync{
		GonatsProxy: goproxy.NewGonatsProxy(),
	}

	c.InitProxy(c)
	return c
}

// Publish 将指定的消息发布到NATS服务器。
// 参数 msg 为要发布的消息体，类型为 *gomsg.GonatsMsg。
// 返回值为执行过程中可能出现的错误。
func (this *CliAsync) Publish(msg *gomsg.GonatsMsg) error {
	goutils.Info(msg)
	return this.PublishMsg(msg.To())

}
