package cliasync

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cli_async_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameCliAsync = "cliasync.CliAsync"

// init register load
func init() {
	registerBeanCliAsync()
}

// register CliAsync
func registerBeanCliAsync() {
	basedi.RegisterLoadBean(singleNameCliAsync, LoadCliAsync)
}

func FindBeanCliAsync() *CliAsync {
	bean, ok :=  basedi.FindBean(singleNameCliAsync).(*CliAsync)
    if !ok {
        logrus.Errorf("FindBeanCliAsync: failed to cast bean to *CliAsync")
        return nil
    }
    return bean

}

func LoadCliAsync() baseiface.ISingleton {
	var s = NewCliAsync()
	InjectCliAsync(s)
	return s

}

func InjectCliAsync(s *CliAsync)    {
    
    logrus.Debug("inject")
}


