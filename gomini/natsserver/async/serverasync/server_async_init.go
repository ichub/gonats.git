package serverasync

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: server_async_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameServerAsync = "serverasync.ServerAsync"

// init register load
func init() {
	registerBeanServerAsync()
}

// register ServerAsync
func registerBeanServerAsync() {
	basedi.RegisterLoadBean(singleNameServerAsync, LoadServerAsync)
}

func FindBeanServerAsync() *ServerAsync {
	bean, ok :=  basedi.FindBean(singleNameServerAsync).(*ServerAsync)
    if !ok {
        logrus.Errorf("FindBeanServerAsync: failed to cast bean to *ServerAsync")
        return nil
    }
    return bean

}

func LoadServerAsync() baseiface.ISingleton {
	var s = NewServerAsync()
	InjectServerAsync(s)
	return s

}

func InjectServerAsync(s *ServerAsync)    {
    
    logrus.Debug("inject")
}


