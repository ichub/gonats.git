package serverasync

import (
	"context"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsgrunner"
	"gitee.com/ichub/gonats/gomini/gonats/goproxy"

	"github.com/nats-io/nats.go"
	"github.com/sirupsen/logrus"
)

type ServerAsync struct {
	basedto.BaseEntitySingle
	*goproxy.GonatsProxy
	TotalMsg int
}

func NewServerAsync() *ServerAsync {

	var server = &ServerAsync{
		GonatsProxy: goproxy.NewGonatsProxy(),
	}

	server.InitChannels()
	server.MsgHandler = server.handleMessage
	server.InitProxy(server)
	return server
}

// subscribe
func (self *ServerAsync) Subscribe(Topic string) (*nats.Conn, error) {

	return self.GonatsProxy.Subscribe(Topic, self.Dispatch)

}

func (self *ServerAsync) StartServer() (*nats.Conn, error) {

	// 异步消息处理
	go self.HandleChannels()
	if conn, err := self.QueueSubscribe(goconsts.Topic_DOMAIN_GENEAL_AsyncES_PRE, goconsts.QUEUE_NAME_ASYNC_DEFAULT, self.Dispatch); err != nil {
		self.Error(err)
		return nil, err
	} else {
		logrus.Info("conn is = ", conn.IsConnected())
		return conn, nil
	}
}

func (self *ServerAsync) handleMessage(msg *nats.Msg) {

	var gonatsmsg, _ = self.BorrowObject(context.Background())
	defer self.ReturnObject(gonatsmsg.From(msg))

	var resp = gomsgrunner.FindBeanGomsgRunner().Execute(gonatsmsg)
	goutils.Info("serverASync handle message resp is ", resp.ToPrettyString())

	//var err = msg.Ack()

}
