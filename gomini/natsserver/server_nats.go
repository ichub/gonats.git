package main

import (
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/goperfstat"
	"gitee.com/ichub/gonats/gomini/natsserver/async/serverasync"
	"gitee.com/ichub/gonats/gomini/natsserver/sync/serversync"
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			goutils.Error("[main pfserver] Recovered in ", r)

		}
	}()
	goutils.Info("now starting serverNats....")
	goperfstat.FindBeanGoperfStat().StartStats()
	//async Server
	var serverAsync = serverasync.FindBeanServerAsync()
	conn, err := serverAsync.StartServer()
	if err != nil || conn == nil {
		goutils.Error(err)
		panic("start serverAsync error!")
	}
	defer conn.Close()

	//sync Server同步
	if err := serversync.FindBeanServerSync().StartServer(); err != nil {
		panic("start serverSync error!")
	}

}
