package main

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	pageesdto "gitee.com/ichub/gomini/mini/general/query/pagees"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"

	"gitee.com/ichub/gonats/gomini/natsserver/async/cliasync"
	"gitee.com/ichub/gonats/gomini/natsserver/sync/clisync"
	"github.com/gogf/gf/util/gconv"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
)

const msgnumber = 1000

var cli = clisync.FindBeanCliSync()

func TestMain(m *testing.M) {

	m.Run() //goleak.VerifyTestMain(m)
}
func makeREQ() *pageesdto.PageesRequest {
	var esRequest = pageesdto.New()
	esRequest.PageSize = 3
	esRequest.IndexName = "ichub_sys_dept"
	esRequest.Source = "dept_id,dept_name" //esRequest.EsTerm("dept_name", "kl")
	return esRequest
}

func Test000_Query(t *testing.T) {

	esQueryResult, err := makeREQ().EsQueryResult()
	if err != nil {
		logrus.Error(err)
	}
	goutils.Info(esQueryResult.ToPrettyString())
	goutils.Info(makeREQ())

}

func Test001_syncCliOneRequestEs(t *testing.T) {

	var msg = gomsg.FindBeanGonatsMsg()
	msg.Body = makeREQ()
	msg.Domain = goconsts.DomainCms
	msg.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS
	msg.Action = goconsts.ES_ACTION_QUERY

	var request, err = cli.Request(msg.Encode())
	if err != nil {
		goutils.Error(err)
		return
	}
	var resp = gomsg.NewResp(request)
	resp.Decode()

	assert.Equal(t, goconsts.DomainCms, resp.Domain)
}

func Test002_asyncCli_publish(t *testing.T) {
	t.Parallel()
	for i := 0; i < msgnumber; i++ {

		var msg = gomsg.FindBeanGonatsMsg()
		msg.ObjectId = gconv.String(baseutils.SnowflakeNextVal())

		msg.Topic = goconsts.Topic_DOMAIN_GENEAL_AsyncES_BIZ
		var result = basedto.NewIchubResult()
		msg.SetData(result)
		msg.MsgReq = true

		var err = cliasync.FindBeanCliAsync().Publish(msg)
		if err != nil {
			goutils.Error(err)
		}
		goutils.Info(msg)
	}

}
func Test003_asyncCli_publish(t *testing.T) {
	t.Parallel()
	for i := 0; i < msgnumber; i++ {

		var msg = gomsg.FindBeanGonatsMsg()
		msg.Header.ObjectId = gconv.String(baseutils.SnowflakeNextVal())

		msg.Header.Topic = goconsts.Topic_DOMAIN_GENEAL_ASyncES_CMS
		var result = basedto.NewIchubResult()
		msg.SetData(result)
		msg.Header.MsgReq = true
		//msg.Parse2Result()

		var err = cliasync.FindBeanCliAsync().Publish(msg)
		if err != nil {
			goutils.Error(err)
		}
		goutils.Info(msg.ToPrettyString())
		goutils.Error(msg.ToPrettyString())
	}

}
func Test004_syncCliTopicARequest(t *testing.T) {
	t.Parallel()
	for i := 0; i < msgnumber; i++ {

		var msg = makeMsg()
		var resp, err = clisync.FindBeanCliSync().Request(msg)
		if err != nil {
			goutils.Error(err)
			continue
		}

		goutils.Info("resp=", jsonutils.ToJsonPretty(resp))
		var msgres = gomsg.NewResp(resp)
		msgres.Decode()
		goutils.Info("result new=", msgres)

	}
}
func makeMsg() *gomsg.GonatsMsg {
	var msg = gomsg.FindBeanGonatsMsg()
	msg.Body = makeReq()
	msg.Domain = goconsts.DomainCms
	msg.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS
	msg.Action = goconsts.ES_ACTION_QUERY
	return msg.Encode()
}
func Test005_syncCliTopicBrequest(t *testing.T) {
	t.Parallel()

	for i := 0; i < msgnumber; i++ {

		var msg = makeMsg().Encode()

		var resp, err = clisync.FindBeanCliSync().Request(msg)
		if err != nil {
			logrus.Error(err)
			continue
		}
		goutils.Info("resp=", jsonutils.ToJsonPretty(resp))
		var msgres = gomsg.NewResp(resp)
		msgres.Decode()
		goutils.Info("result new=", msgres)
		//assert.Equal(t, "abc"+gconv.String(i+1000), string(msgres.Data))
		assert.Equal(t, goconsts.DomainGeneral, msgres.Header.Domain)
	}
}

const pagereq = `
{
  "page_size": 2,
  "current": 1,
  "order_by": null,
  "fields": [
    {
      "field": "email",
      "op_type": "like",
      "values": [
        "leijmdas@163.com"
      ]
    },
    {
      "field": "dept_id",
      "op_type": "between",
      "values": [
        "553270041122963456",
        "553270041122963456"
      ]
    }
  ],
  "index_name": "ichub_sys_dept"
} `

func Test006_syncCliTopicOne(t *testing.T) {

	var msg = gomsg.FindBeanGonatsMsg()

	msg.Header.ObjectId = "leijmdas" + gconv.String(100000)
	msg.Data = []byte("leijmdas" + gconv.String(1000))

	msg.Body = makeReq()
	msg.Encode()

	msg.Header.MsgReq = true
	msg.Header.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS

	var resp, err = clisync.FindBeanCliSync().Request(msg)
	if err != nil {
		logrus.Error(err)
		return
	}
	goutils.Info("resp=", jsonutils.ToJsonPretty(resp))
	var msgres = gomsg.NewResp(resp)
	msgres.Decode()
	goutils.Info("result new=", msgres)
	//assert.Equal(t, "abc"+gconv.String(i+1000), string(msgres.Data))
	assert.Equal(t, goconsts.DomainGeneral, msgres.Header.Domain)

}

func Test007_findPkgDir(t *testing.T) {
	var p, err = fileutils.GetPkgPath("gitee.com/ichub/di/di")
	if err != nil {
		goutils.Error(err)
	}
	goutils.Info(p)

}
