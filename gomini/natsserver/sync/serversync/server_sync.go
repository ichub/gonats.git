package serversync

import (
	"context"
	"errors"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsgrunner"
	"gitee.com/ichub/gonats/gomini/gonats/goproxy"

	"github.com/gogf/gf/util/gconv"
	"github.com/nats-io/nats.go"
	"github.com/sirupsen/logrus"
	"time"
)

// ServerSync 是一个请求响应服务器结构体，继承了ReqRespBase和basedto.BaseEntitySingle。

const (
	// RetryDelay 初始重试延迟，用于消息处理时的重试策略
	RetryDelay = time.Millisecond
	// MaxRetryAttempts 最大重试次数
	MaxRetryAttempts = 1
)

type ServerSync struct {
	basedto.BaseEntitySingle

	*goproxy.GonatsProxy
}

func NewServerSync() *ServerSync {

	var server = &ServerSync{
		GonatsProxy: goproxy.NewGonatsProxy(),
	}
	server.InitChannels()
	server.MsgHandler = server.handleMessage
	server.InitProxy(server)
	return server
}

func (self *ServerSync) StartServer() error {
	self.WaitGroup.Add(1)
	go self.HandleChannels()
	var err = self.start(goconsts.Topic_DOMAIN_GENEAL_SyncES_PRE, goconsts.QUEUE_NAME_SYNC_DEFAULT)
	if err != nil {
		self.WaitGroup.Done()
		return err
	}
	self.WaitGroup.Wait()
	return nil
}

func (self *ServerSync) start(topic string, queueName string) error {
	if !self.CheckTimeout() {
		return errors.New("timeout is invalid!  " + gconv.String(self.Timeout))
	}
	defer self.WaitGroup.Done()

	conn, err := self.Conn()
	if err != nil {
		goutils.Error(err)
		return err
	}
	defer conn.Close()

	// 创建一个Subscription来监听特定的Subject
	sub, err := conn.QueueSubscribeSync(topic, queueName)
	if err != nil {
		goutils.Error(err)
		return err
	}
	defer func() {
		var err = sub.Unsubscribe()
		if err != nil {
			logrus.Error("Error unsubscribing: %v", err)
		}
	}()
	conn.Flush()
	goutils.Info("Starting serverSync, Waiting for request msg from client on topic : ", topic)
	goutils.Info("conn is ", conn.IsConnected())
	// 创建一个请求响应循环
	for {
		// 等待请求消息
		msg, err := sub.NextMsg(self.ITimeout)
		if err != nil {
			time.Sleep(time.Millisecond)
			continue
		}
		// 响应消息 msg.RespondMsg(m.To())
		self.Dispatch(msg)
		//self.Info(context.Background(), "serverSync receive request message from client is ", gomsg.ToGonatsMsg(msg).ToPrettyString())
	}
	//conn.Loop()
	return nil
}

func (self *ServerSync) handleMessage(msg *nats.Msg) {

	var gonatsmsg, _ = self.BorrowObject(context.Background())
	defer self.ReturnObject(gonatsmsg.From(msg))
	var resp = gomsgrunner.FindBeanGomsgRunner().Execute(gonatsmsg)
	if err := msg.RespondMsg(resp.To()); err != nil {
		goutils.Error(err)
		self.Info("serverSync handleMessage and send Resp to client error is ", resp)
		return
	}
	self.Info("serverSync handleMessage and send Resp to client is ", resp)
	//var err = msg.AckSync()
}
