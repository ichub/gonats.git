package serversync

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: server_sync_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameServerSync = "serversync.ServerSync"

// init register load
func init() {
	registerBeanServerSync()
}

// register ServerSync
func registerBeanServerSync() {
	basedi.RegisterLoadBean(singleNameServerSync, LoadServerSync)
}

func FindBeanServerSync() *ServerSync {
	bean, ok :=  basedi.FindBean(singleNameServerSync).(*ServerSync)
    if !ok {
        logrus.Errorf("FindBeanServerSync: failed to cast bean to *ServerSync")
        return nil
    }
    return bean

}

func LoadServerSync() baseiface.ISingleton {
	var s = NewServerSync()
	InjectServerSync(s)
	return s

}

func InjectServerSync(s *ServerSync)    {
    
    logrus.Debug("inject")
}


