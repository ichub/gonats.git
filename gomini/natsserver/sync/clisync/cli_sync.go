package clisync

import (
	"context"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"gitee.com/ichub/gonats/gomini/gonats/goproxy"

	"github.com/gogf/gf/util/gconv"
	"github.com/nats-io/nats.go"
)

type CliSync struct {
	basedto.BaseEntitySingle
	*goproxy.GonatsProxy
}

func NewCliSync() *CliSync {
	var c = &CliSync{
		GonatsProxy: goproxy.NewGonatsProxy(),
	}
	c.InitProxy(c)
	return c
}

func (self *CliSync) Request(msg *gomsg.GonatsMsg) (*nats.Msg, error) {
	if err := msg.Check(); err != nil {
		goutils.Error(err.Error())
		return nil, err
	}
	return self.RequestMsg(msg.To())

}

func (self *CliSync) RequestMsg(req *nats.Msg) (*nats.Msg, error) {

	nc, err := self.Conn()
	if err != nil {
		self.Error(err)
		return nil, err
	}
	defer nc.Close()

	goutils.Info(context.Background(), "cliSync client send request message to server is ", req)

	if resp, err := nc.RequestMsg(req, gconv.Duration(self.ITimeout)); err != nil {
		goutils.FindBeanGoLogcli().Info(context.Background(), err.Error())
		return nil, err
	} else {

		goutils.Info(context.Background(), "cliSync client receive resp from server is ", resp)
		return resp, nil
	}
}
