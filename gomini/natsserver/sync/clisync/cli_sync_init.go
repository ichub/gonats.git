package clisync

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: cli_sync_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameCliSync = "clisync.CliSync"

// init register load
func init() {
	registerBeanCliSync()
}

// register CliSync
func registerBeanCliSync() {
	basedi.RegisterLoadBean(singleNameCliSync, LoadCliSync)
}

func FindBeanCliSync() *CliSync {
	bean, ok :=  basedi.FindBean(singleNameCliSync).(*CliSync)
    if !ok {
        logrus.Errorf("FindBeanCliSync: failed to cast bean to *CliSync")
        return nil
    }
    return bean

}

func LoadCliSync() baseiface.ISingleton {
	var s = NewCliSync()
	InjectCliSync(s)
	return s

}

func InjectCliSync(s *CliSync)    {
    
    logrus.Debug("inject")
}


