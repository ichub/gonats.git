package main

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/gomini/mini/general/query/page"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"

	"gitee.com/ichub/gonats/gomini/natsserver/sync/clisync"
	"github.com/gogf/gf/util/gconv"
	"github.com/nats-io/nats.go"
	"github.com/sirupsen/logrus"
	"testing"
	"time"
)

func Test001_clientTopicA(t *testing.T) {

	for i := 0; i < 100; i++ {

		var msg = gomsg.FindBeanGonatsMsg()
		msg.Header.ObjectId = gconv.String(baseutils.SnowflakeNextVal())
		var result = basedto.NewIchubResult()
		msg.SetData(result)
		msg.Header.MsgReq = true
		var resp, err = clisync.FindBeanCliSync().Request(msg)
		if err != nil {
			goutils.Error(err)
			continue
		}

		goutils.Info("resp=", jsonutils.ToJsonPretty(resp))
		var msgres = gomsg.NewGonatsMsgresp()
		msgres.From(resp)
		msgres.Parse2Result()
		goutils.Info("result new=", msgres)

	}
}

const pagereq = `
{
  "page_size": 2,
  "current": 1,
  "order_by": null,
  "fields": [
    {
      "field": "email",
      "op_type": "like",
      "values": [
        "leijmdas@163.com"
      ]
    },
    {
      "field": "dept_id",
      "op_type": "between",
      "values": [
        "553270041122963456",
        "553270041122963456"
      ]
    }
  ],
  "index_name": "ichub_sys_dept"
} `

func Test002_clientTopicB(t *testing.T) {

	for i := 0; i < 110; i++ {

		var msg = gomsg.FindBeanGonatsMsg()
		var ii = 1 + 100000 + i
		msg.Header.ObjectId = "abc" + gconv.String(100000+ii)
		msg.Data = []byte("abc" + gconv.String(ii))
		var result = page.NewIchubPageResult()
		msg.SetData(result)
		msg.SetData(pagereq)

		msg.Header.MsgReq = true
		msg.Header.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS

		var resp, err = clisync.FindBeanCliSync().Request(msg)
		if err != nil {
			logrus.Error(err)
			continue
		}
		goutils.Info("resp=", jsonutils.ToJsonPretty(resp))
		var msgres = gomsg.NewGonatsMsgresp()
		msgres.From(resp)
		msgres.DecodeData(page.NewIchubPageResult())
		goutils.Info("result new=", msgres)

	}
}

func Test003_nats_request(t *testing.T) {
	nc, err := nats.Connect("natsserver://huawei.akunlong.top:4222")
	if err != nil {
		logrus.Error(err)
	}
	defer nc.Close()

	request := []byte("Hello NATS!")
	//var response []byte
	subj, err := nc.Request("request.Topic", request, 30*time.Second)
	if err != nil {
		logrus.Error(err)
		return
	}
	logrus.Println(string(subj.Data))
}
