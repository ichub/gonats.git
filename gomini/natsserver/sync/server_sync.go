package main

import (
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/goperfstat"

	"gitee.com/ichub/gonats/gomini/natsserver/sync/serversync"
)

func main() {
	goutils.Info("starting serverSync....")
	goperfstat.FindBeanGoperfStat().StartStats()
	var serverSync = serversync.FindBeanServerSync()
	serverSync.StartServer()

	serverSync.CloseChannels()
}
