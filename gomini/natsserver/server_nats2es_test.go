package main

import (
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	pageesdto "gitee.com/ichub/gomini/mini/general/query/pagees"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"github.com/stretchr/testify/assert"
	"testing"
)

func makeReq() *pageesdto.PageesRequest {
	var esRequest = pageesdto.New()
	esRequest.PageSize = 3
	esRequest.IndexName = "ichub_sys_dept"
	esRequest.Source = "dept_id,dept_name"
	return esRequest
}

func Test001_es_Query(t *testing.T) {

	esQueryResult, err := makeReq().EsQueryResult()
	if err != nil {
		goutils.Error(err)
	}
	goutils.Info(esQueryResult.ToPrettyString(), makeReq())

}

func Test002_es_syncOneRequest(t *testing.T) {

	var msg = gomsg.FindBeanGonatsMsg()
	msg.Body = makeReq()
	msg.Domain = goconsts.DomainCms
	msg.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS
	msg.Action = goconsts.ES_ACTION_QUERY

	var result, err = cli.Request(msg.Encode())
	if err != nil {
		goutils.Error(err)
		return
	}
	var resp = gomsg.NewResp(result)
	resp.Decode()

	assert.Equal(t, goconsts.DomainCms, resp.Domain)
}
func Test003_es_syncOneRequest(t *testing.T) {

	var msg = gomsg.FindBeanGonatsMsg()
	msg.Body = makeReq()
	msg.Domain = goconsts.DomainCms
	msg.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS
	msg.Action = goconsts.ES_ACTION_QUERY

	var result, err = cli.Request(msg.Encode())
	if err != nil {
		goutils.Error(err)
		return
	}
	var resp = gomsg.NewResp(result)
	resp.Decode()

	assert.Equal(t, goconsts.DomainCms, resp.Domain)
}
func Test003_es_syncOneRequestCheck(t *testing.T) {

	var msg = gomsg.FindBeanGonatsMsg()
	msg.Body = makeReq()
	msg.Domain = goconsts.DomainCms + "3"
	msg.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS
	msg.Action = goconsts.ES_ACTION_QUERY

	var result, err = cli.Request(msg.Encode())
	if err != nil {
		goutils.Error(err)
		return
	}
	var resp = gomsg.NewResp(result)
	resp.Decode()

	assert.Equal(t, goconsts.DomainCms, resp.Domain)
}
func Test004_es_syncOneRequestNotExists(t *testing.T) {
	var make = func() *pageesdto.PageesRequest {
		var esRequest = pageesdto.New()
		esRequest.PageSize = 1
		esRequest.IndexName = "ichub_sys_dept1"
		esRequest.Source = "dept_id,dept_name" //esRequest.EsTerm("dept_name", "kl")
		return esRequest
	}

	var msg = gomsg.FindBeanGonatsMsg()
	msg.Body = make()
	msg.Domain = goconsts.DomainCms
	msg.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS
	msg.Action = goconsts.ES_ACTION_QUERY

	var request, err = cli.Request(msg.Encode())
	if err != nil {
		goutils.Error(err)
		return
	}
	var resp = gomsg.NewResp(request)
	resp.Decode()

	assert.Equal(t, goconsts.DomainCms, resp.Domain)
}
