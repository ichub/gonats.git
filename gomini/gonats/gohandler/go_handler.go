package gohandler

import (
	"context"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/goevent/eventcontainer"
	"gitee.com/ichub/gonats/gomini/goevent/eventiface"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
)

// 初始化函数，在包加载时执行，用于注册Nats事件处理器
func init() {
	eventcontainer.RegisterHandlerNats(goconsts.Topic_DOMAIN_GENEAL_SyncES_PRE, NewGoHandler())
	eventcontainer.RegisterHandlerNats(goconsts.Topic_DOMAIN_GENEAL_AsyncES_PRE, NewGoHandler())
}

func Execute(eventContext eventiface.EventContext) {

	gonatsMsg := eventContext.FindGonatsMsg()
	var handlerNats = eventcontainer.FindHandlerNats(gonatsMsg.Header.Topic2Pre())
	if handlerNats != nil {
		handlerNats.Execute(eventContext)
	} else {
		goutils.Error(context.Background(), "handlerNats is nil")

	}
}

type GoHandler struct {
	basedto.BaseEntitySingle
}

func NewGoHandler() *GoHandler {
	return &GoHandler{}
}

func (this *GoHandler) Execute(eventContext eventiface.EventContext) {
	eventContext.Execute()
}
