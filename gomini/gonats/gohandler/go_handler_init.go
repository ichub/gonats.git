package gohandler

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_handler_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGoHandler = "gohandler.GoHandler"

// init register load
func init() {
	registerBeanGoHandler()
}

// register GoHandler
func registerBeanGoHandler() {
	basedi.RegisterLoadBean(singleNameGoHandler, LoadGoHandler)
}

func FindBeanGoHandler() *GoHandler {
	bean, ok :=  basedi.FindBean(singleNameGoHandler).(*GoHandler)
    if !ok {
        logrus.Errorf("FindBeanGoHandler: failed to cast bean to *GoHandler")
        return nil
    }
    return bean

}

func LoadGoHandler() baseiface.ISingleton {
	var s = NewGoHandler()
	InjectGoHandler(s)
	return s

}

func InjectGoHandler(s *GoHandler)    {
    
    logrus.Debug("inject")
}


