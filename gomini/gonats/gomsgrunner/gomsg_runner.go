package gomsgrunner

import (
	"fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	pagees "gitee.com/ichub/gomini/mini/general/query/pagees"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"gitee.com/ichub/gonats/gomini/goperfstat"
)

// 定义错误代码常量
const ErrorCodeInternal = 500

// GomsgRunner 结构体，实现复杂的业务逻辑，并返回结果
type GomsgRunner struct {
	basedto.BaseEntitySingle
}

func NewGonatsMsgexecutor() *GomsgRunner {
	return &GomsgRunner{}
}

// processRequest 对请求进行处理的通用函数
func (this *GomsgRunner) Stat(msg *gomsg.GonatsMsg) *gomsg.GonatsMsgresp {
	var action = "stat"
	_, ok := msg.Body.(*pagees.PageesRequest)
	if !ok {
		goutils.Error(fmt.Sprintf("GomsgRunner %s req is not pageesdto.PageesRequest", action))
		return gomsg.EsNotSupport(msg)
	}
	stat := goperfstat.FindBeanGoperfStat().FindStatMap()
	var result = basedto.NewIchubResult().SuccessData(stat)
	resp := gomsg.NewRespMsg(msg)
	resp.Body = result
	resp.Encode()
	return resp
}
func (this *GomsgRunner) processRequest(msg *gomsg.GonatsMsg, action string) *gomsg.GonatsMsgresp {
	req, ok := msg.Body.(*pagees.PageesRequest)
	if !ok {
		goutils.Error(fmt.Sprintf("GomsgRunner %s req is not pageesdto.PageesRequest", action))
		return gomsg.EsNotSupport(msg)
	}
	queryResult, err := req.EsQueryResult()
	if err != nil {
		goutils.Error(fmt.Sprintf("GomsgRunner %s error: %v", action, err))
		queryResult.Msg = fmt.Sprintf("%s error", action)
		queryResult.Code = ErrorCodeInternal
	}
	resp := gomsg.NewRespMsg(msg)
	resp.Body = queryResult
	resp.Encode()
	return resp
}
func (this *GomsgRunner) EsQueryResult(msg *gomsg.GonatsMsg) *gomsg.GonatsMsgresp {
	return this.processRequest(msg, goconsts.ES_ACTION_QUERY)
}

func (this *GomsgRunner) Check(msg *gomsg.GonatsMsg) error {
	return FindBeanGomsgCheck().Check(msg)

}
func (this *GomsgRunner) Execute(msg *gomsg.GonatsMsg) *gomsg.GonatsMsgresp {
	if err := msg.Decode(); err != nil {
		goperfstat.FindBeanGoperfStat().Trigger(msg.Topic, false)
		goutils.Error(err)
		return gomsg.EsNotSupport(msg)
	}
	goperfstat.FindBeanGoperfStat().Trigger(msg.Topic, true)

	goutils.Debug("GonatsMsg Execute req is ", msg)
	return this.Exec(msg)
}

func (this *GomsgRunner) Exec(msg *gomsg.GonatsMsg) *gomsg.GonatsMsgresp {
	goutils.Info("GomsgRunner Execute here ...")
	if err := this.Check(msg); err != nil {
		return gomsg.EsErrorResp(msg, err)
	}

	switch msg.Header.Action {
	case goconsts.ES_ACTION_QUERY:
		return this.EsQueryResult(msg)
	case goconsts.ES_ACTION_CMD:
		return this.EsCmd(msg)
	case goconsts.ES_ACTION_METADATA:
		return this.EsMetadata(msg)
	case goconsts.ES_ACTION_KEYWORD:
		return this.EsKeyword(msg)

	case goconsts.ES_ACTION_STAT:
		return this.Stat(msg)
	default:

		return gomsg.EsNotSupport(msg)
	}
}

func (this *GomsgRunner) EsCmd(msg *gomsg.GonatsMsg) *gomsg.GonatsMsgresp {
	return this.processRequest(msg, "EsCmd")
}

func (this *GomsgRunner) EsKeyword(msg *gomsg.GonatsMsg) *gomsg.GonatsMsgresp {
	return this.processRequest(msg, "EsKeyword")
}

func (this *GomsgRunner) EsMetadata(msg *gomsg.GonatsMsg) *gomsg.GonatsMsgresp {
	return this.processRequest(msg, "EsMetadata")
}
