package gomsgrunner

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gomsg_check_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGomsgCheck = "gomsgrunner.GomsgCheck"

// init register load
func init() {
	registerBeanGomsgCheck()
}

// register GomsgCheck
func registerBeanGomsgCheck() {
	basedi.RegisterLoadBean(singleNameGomsgCheck, LoadGomsgCheck)
}

func FindBeanGomsgCheck() *GomsgCheck {
	bean, ok :=  basedi.FindBean(singleNameGomsgCheck).(*GomsgCheck)
    if !ok {
        logrus.Errorf("FindBeanGomsgCheck: failed to cast bean to *GomsgCheck")
        return nil
    }
    return bean

}

func LoadGomsgCheck() baseiface.ISingleton {
	var s = NewGomsgCheck()
	InjectGomsgCheck(s)
	return s

}

func InjectGomsgCheck(s *GomsgCheck)    {
    
    logrus.Debug("inject")
}


