package gomsgrunner

import (
	"errors"
	"fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"strings"
)

// 定义错误消息常量
const (
	errMsgDomainEmpty   = "domain is empty"
	errMsgTopicEmpty    = "topic is empty"
	errMsgActionEmpty   = "action is empty"
	errMsgTopicInvalid  = "topic is invalid"
	errMsgDomainInvalid = "domain is invalid"
)

type GomsgCheck struct {
	basedto.BaseEntitySingle
}

func NewGomsgCheck() *GomsgCheck {
	return &GomsgCheck{}
}

// Check 检查消息
func (g *GomsgCheck) Check(msg *gomsg.GonatsMsg) error {
	if msg == nil {
		return errors.New("message is nil")
	}
	return g.CheckHeader(msg)
}

// CheckHeader 检查消息头
func (g *GomsgCheck) CheckHeader(msg *gomsg.GonatsMsg) error {
	if err := g.CheckDomain(msg); err != nil {
		return err
	}
	if err := g.CheckTopic(msg); err != nil {
		return err
	}
	return g.CheckEmpty(msg)
}

// CheckEmpty 检查消息字段是否为空
func (g *GomsgCheck) CheckEmpty(msg *gomsg.GonatsMsg) error {
	if msg.Domain == "" {
		return fmt.Errorf(errMsgDomainEmpty)
	}
	if msg.Topic == "" {
		return fmt.Errorf(errMsgTopicEmpty)
	}
	if msg.Action == "" {
		return fmt.Errorf(errMsgActionEmpty)
	}

	return nil
}

// CheckTopic 检查话题有效性
func (g *GomsgCheck) CheckTopic(msg *gomsg.GonatsMsg) error {
	if !strings.Contains(msg.Topic, ".") {
		return fmt.Errorf(errMsgTopicInvalid+": %s", msg.Topic)
	}
	return nil
}

// CheckDomain 检查域名有效性
func (g *GomsgCheck) CheckDomain(msg *gomsg.GonatsMsg) error {
	if msg.Domain != goconsts.DomainCms && msg.Domain != goconsts.DomainGeneral {
		return fmt.Errorf(errMsgDomainInvalid+": %s", msg.Domain)
	}
	return nil
}
