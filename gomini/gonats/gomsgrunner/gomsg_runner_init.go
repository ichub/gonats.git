package gomsgrunner

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gomsg_runner_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGomsgRunner = "gomsgrunner.GomsgRunner"

// init register load
func init() {
	registerBeanGomsgRunner()
}

// register GomsgRunner
func registerBeanGomsgRunner() {
	basedi.RegisterLoadBean(singleNameGomsgRunner, LoadGomsgRunner)
}

func FindBeanGomsgRunner() *GomsgRunner {
	bean, ok :=  basedi.FindBean(singleNameGomsgRunner).(*GomsgRunner)
    if !ok {
        logrus.Errorf("FindBeanGomsgRunner: failed to cast bean to *GomsgRunner")
        return nil
    }
    return bean

}

func LoadGomsgRunner() baseiface.ISingleton {
	var s = NewGonatsMsgexecutor()
	InjectGomsgRunner(s)
	return s

}

func InjectGomsgRunner(s *GomsgRunner)    {
    
    logrus.Debug("inject")
}


