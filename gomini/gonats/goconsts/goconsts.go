package goconsts

import (
	"time"
)

// 读取环境变量来提高安全性和灵活性
const (
	NatsUrl = "nats://huawei.akunlong.top:4222" //os.Getenv("NATS_URL")
)

const (
	CHANNEL_NUMBER  = 10
	ChanSize        = 100
	RequestWaitTime = 30 * time.Second
	NextMsgTimeout  = 10 * time.Second
)
const (
	//性能统计
	TopicGeneralSyncSTAT = "GeneralEsSync.SYSTEM.STAT"
	// 连接信息
	Topic_DOMAIN_GENEAL_ASyncCONN     = "GeneralEsSync.SYSTEM.CONN"
	Topic_DOMAIN_GENEAL_ASync_DISCONN = "GeneralEsSync.SYSTEM.DISCONN"
	//性能统计通知消息
	Topic_Async_Notify_PRE = "GeneralDefault.*"
	Topic_Async_NotifyCMS  = "GeneralDefault.CMS"
)

// 通过环境变量或配置文件管理敏感信息，这里仅为示例
const (
	QUEUE_NAME_SYNC_DEFAULT  = "QueueSyncGeneralEs"
	QUEUE_NAME_ASYNC_DEFAULT = "QueueAsyncGeneralEs"
	Topic_Async_Default      = "GeneralEsSync.DEFAULT"

	Topic_DOMAIN_GENEAL_SyncES_PRE = "GeneralEsSync.*"

	Topic_DOMAIN_GENEAL_SyncES_BIZ = "GeneralEsSync.BIZ"
	Topic_DOMAIN_GENEAL_SyncES_CMS = "GeneralEsSync.CMS"

	Topic_DOMAIN_GENEAL_AsyncES_PRE = "GeneralEsAsync.*"
	Topic_DOMAIN_GENEAL_AsyncES_BIZ = "GeneralEsAsync.BIZ"
	Topic_DOMAIN_GENEAL_ASyncES_CMS = "GeneralEsAsync.CMS"
)
