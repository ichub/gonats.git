package goconsts

const (
	DomainGeneral   = "general" //通用域 general
	DomainOry       = "ory"
	DomainPortal    = "portal"    //人脉域 ory	/*门户域 portal*/
	DomainAccount   = "account"   //账户域 account
	DomainCms       = "cms"       //内容域 cms
	DomainHub       = "hub"       //合约域 hub
	DomainBiz       = "biz"       //业务域 bizframe
	DomainOrder     = "order"     //订单域 order
	DomainLogistics = "logistics" //物流域 logistics
	DomainFinance   = "finance"   //财务域 finance
	DomainOperating = "operating" // 营运域
	DomainProduct   = "product"   //产品域 product*/
)

const (
	ES_ACTION_STAT = "stat"

	ES_ACTION_QUERY     = "query"
	ES_ACTION_AGGREGATE = "agg"
	ES_ACTION_CMD       = "cmd"
	ES_ACTION_KEYWORD   = "keyword"
	ES_ACTION_METADATA  = "metadata"
)
