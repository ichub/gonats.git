package goproxy

import (
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"github.com/nats-io/nats.go"
)

type IGonatsProxySync interface {
	Request(msg *gomsg.GonatsMsg) *gomsg.GonatsMsg
	SubscribeSync(topic string) (*nats.Subscription, error)
	Conn() (*nats.Conn, error)
}
