package goproxy

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gonats_proxy_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGonatsProxy = "goproxy.GonatsProxy"

// init register load
func init() {
	registerBeanGonatsProxy()
}

// register GonatsProxy
func registerBeanGonatsProxy() {
	basedi.RegisterLoadBean(singleNameGonatsProxy, LoadGonatsProxy)
}

func FindBeanGonatsProxy() *GonatsProxy {
	bean, ok :=  basedi.FindBean(singleNameGonatsProxy).(*GonatsProxy)
    if !ok {
        logrus.Errorf("FindBeanGonatsProxy: failed to cast bean to *GonatsProxy")
        return nil
    }
    return bean

}

func LoadGonatsProxy() baseiface.ISingleton {
	var s = NewGonatsProxy()
	InjectGonatsProxy(s)
	return s

}

func InjectGonatsProxy(s *GonatsProxy)    {
    
    logrus.Debug("inject")
}


