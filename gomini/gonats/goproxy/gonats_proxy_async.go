package goproxy

import (
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"github.com/nats-io/nats.go"
)

type IGonatsProxyAsync interface {
	PublishMsg(msg *nats.Msg) error
	QueueSubscribe(topic string, queueName string, handler nats.MsgHandler) (*nats.Conn, error)

	Request(msg *gomsg.GonatsMsg) *gomsg.GonatsMsg
	SubscribeSync(topic string) (*nats.Subscription, error)
	Conn() (*nats.Conn, error)
}
