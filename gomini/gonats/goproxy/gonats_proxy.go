package goproxy

import (
	"context"
	"fmt"
	"gitee.com/ichub/goconfig/common/base/baseconfig"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/goconfig/common/ichubcontext"
	"gitee.com/ichub/gonats/gomini/goevent/eventiface"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"gitee.com/ichub/gonats/gomini/gonats/gomsgpool"
	"github.com/sirupsen/logrus"

	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/util/grand"
	"github.com/nats-io/nats.go"
	"sync"
	"time"
)

type GonatsProxy struct {
	basedto.BaseEntitySingle

	*baseconfig.NatsClientDto
	Channels []chan *nats.Msg
	ITimeout time.Duration

	WaitGroup  sync.WaitGroup  `json:"-"`
	lock       sync.Mutex      `json:"-"`
	MsgHandler nats.MsgHandler `json:"-"`
}

func NewGonatsProxy() *GonatsProxy {
	var p = &GonatsProxy{
		Channels: make([]chan *nats.Msg, goconsts.CHANNEL_NUMBER),
	}
	p.InitProxy(p)
	return p.init()
}
func (gp *GonatsProxy) init() *GonatsProxy {
	gp.NatsClientDto = gp.GetNatsDto()
	gp.ITimeout = time.Duration(gconv.Int(gp.Timeout)) * time.Second

	return gp
}
func (gp *GonatsProxy) Error(v ...interface{}) *GonatsProxy {
	goutils.Error(v...)
	goutils.Info(v...)
	return gp
}
func (gp *GonatsProxy) Info(v ...interface{}) *GonatsProxy {
	goutils.Info(v...)
	return gp
}

func (gp *GonatsProxy) Debug(v ...interface{}) *GonatsProxy {
	goutils.Debug(v...)
	return gp
}
func (gp *GonatsProxy) Stat(v ...interface{}) *GonatsProxy {
	goutils.Stat(v...)
	return gp
}

func (gp *GonatsProxy) CloseChannels() {
	for i := 0; i < goconsts.CHANNEL_NUMBER; i++ {
		close(gp.Channels[i])
	}
}
func (gp *GonatsProxy) InitChannels() {
	for i := 0; i < goconsts.CHANNEL_NUMBER; i++ {
		gp.Channels[i] = make(chan *nats.Msg, goconsts.ChanSize)
	}
}

func (gp *GonatsProxy) GetNatsDto() *baseconfig.NatsClientDto {
	return ichubcontext.IchubClient.GetNatsDto()
}

func (gp *GonatsProxy) Request(msg *gomsg.GonatsMsg) *gomsg.GonatsMsg {
	return nil

}
func (gp *GonatsProxy) SubscribeSync(topic string) (*nats.Subscription, error) {
	return nil, nil
}

func (gp *GonatsProxy) PublishMsg(msg *nats.Msg) error {
	var conn, err = gp.Conn()
	if err != nil {
		return err
	}
	defer conn.Close()

	goutils.Info("conn true?", conn.IsConnected())
	if err := conn.PublishMsg(msg); err != nil {
		logrus.Error(err)
		return err
	}
	conn.Flush()
	return nil
}

func (gp *GonatsProxy) PublishData(topic string, data []byte) error {
	var conn, err = gp.Conn()
	if err != nil {
		gp.Error(err)
		return err
	}
	defer conn.Close()
	return conn.Publish(topic, data)
}

func (gp *GonatsProxy) QueueSubscribe(topic string, queueName string, handler nats.MsgHandler) (*nats.Conn, error) {
	var conn, err = gp.Conn()
	if err != nil {
		return nil, err
	}
	//defer conn.Close()

	if r, err := conn.QueueSubscribe(topic, queueName, handler); err != nil {
		gp.Info(r)
	}

	return conn, err
}
func (gp *GonatsProxy) Subscribe(topic string, handler nats.MsgHandler) (*nats.Conn, error) {
	var conn, err = gp.Conn()
	if err != nil {
		return nil, err
	}
	//defer conn.Close()
	if subscribe, err := conn.Subscribe(topic, handler); err != nil {
		gp.Error(subscribe)
	}

	return conn, err
}

func (gp *GonatsProxy) Unsubscribe(topic string) error {
	//conn.Unsubscribe(subject)
	return nil
}

func (gp *GonatsProxy) Conn() (*nats.Conn, error) {
	timeout := gp.ITimeout

	// 创建连接选项并设置超时
	opts := nats.Options{
		AllowReconnect:       true,
		MaxReconnect:         10,
		ReconnectWait:        2 * time.Second,
		Timeout:              timeout,
		Url:                  gp.Url,
		RetryOnFailedConnect: true,

		ConnectedCB: func(conn *nats.Conn) {
			fmt.Printf("Connected to %v ok!\n", conn.ConnectedUrl())
		},
		ClosedCB: func(nc *nats.Conn) {
			fmt.Printf("Connection Closed\n")
		},
		ReconnectedCB: func(nc *nats.Conn) {
			fmt.Printf("Reconnected to %v\n", nc.ConnectedUrl())
		},
		// 还可以设置一个关闭处理器，用于处理连接关闭的情况
		DisconnectedErrCB: func(nc *nats.Conn, err error) {
			fmt.Printf("Disconnected from %v\n", nc.ConnectedUrl())
			if err != nil {
				logrus.Error(err.Error())
			}
		},
	}
	// 尝试创建NATS连接
	nc, err := opts.Connect() //nc, err := natsserver.Connect(gp.Url)
	if err != nil {
		gp.Error("Error connecting to NATS: ", gp.Url)
		return nil, err
	}
	gp.Info("connect nats ", gp.Url)
	return nc, err
}

func (gp *GonatsProxy) CheckTimeout() bool {
	if gp.Timeout == "" {
		return false
	}

	if gp.ITimeout <= 0 {
		return false
	}
	if gp.ITimeout > 10000*time.Second {

		return false
	}
	return true
}
func (gp *GonatsProxy) CheckChannels() bool {

	for _, ch := range gp.Channels {
		if len(ch) > 0 {
			return true
		}
	}
	return false
}

func (gp *GonatsProxy) Execute(ctx eventiface.EventContext) {
	ctx.Execute()
}

func (gp *GonatsProxy) BorrowObject(ctx context.Context) (*gomsg.GonatsMsg, error) {
	return gomsgpool.FindBeanGonatsMsgPool().BorrowObject(ctx)

}
func (gp *GonatsProxy) ReturnObject(msg *gomsg.GonatsMsg) {
	gomsgpool.FindBeanGonatsMsgPool().ReturnObject(context.Background(), msg)

}

func (gp *GonatsProxy) ChooseChannel(objectId string) int {

	//return msg.Header.ChooseChannel()
	return grand.N(0, 9)

}

func (gp *GonatsProxy) Dispatch(msg *nats.Msg) {
	gp.lock.Lock()
	defer gp.lock.Unlock()

	i := gp.ChooseChannel(msg.Header.Get("object_id"))
	gp.Channels[i] <- msg

}

func (gp *GonatsProxy) HandleChannels() {
	for {
		for i := 0; i < goconsts.CHANNEL_NUMBER; i++ {
			gp.WaitGroup.Add(1)
			go gp.handleChannel(gp.Channels[i])

		}
		gp.WaitGroup.Wait()
		time.Sleep(time.Millisecond)
	}
}

func (gp *GonatsProxy) handleChannel(ch chan *nats.Msg) {
	defer gp.WaitGroup.Done()

	for msg := range ch {
		if gp.MsgHandler != nil {

			gp.MsgHandler(msg)
		} else {
			gp.Error("handlerMsg is nil")
		}

	}
}
