package gomsgiface

import (
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"github.com/nats-io/nats.go"
)

type IGomsg interface {
	To() *nats.Msg

	Check() error
	SetData(model interface{}) error
	DecodeData(out interface{}) error
	Parse2PageResult()
	Parse2Result()
	Execute() *gomsg.GonatsMsg
	//eventiface.EventContext

	Decode() error
	Encode() *gomsg.GonatsMsg
}
