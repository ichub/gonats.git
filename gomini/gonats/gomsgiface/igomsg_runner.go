package gomsgiface

import "gitee.com/ichub/gonats/gomini/gonats/gomsg"

type IGomsgRunner interface {
	Execute(msg *gomsg.GonatsMsg) *gomsg.GonatsMsg
	IGomsgCheck
}
