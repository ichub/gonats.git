package gomsgiface

import "gitee.com/ichub/gonats/gomini/gonats/gomsg"

type IGomsgCheck interface {
	Check(msg *gomsg.GonatsMsg) error
}
