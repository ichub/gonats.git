package gomsgpool

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/goconfig/common/basedi"
	"github.com/sirupsen/logrus"
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gonats_msg_pool_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGonatsMsgPool = "gomsgpool.GonatsMsgPool"

// init register load
func init() {
	registerBeanGonatsMsgPool()
}

// register GonatsMsgPool
func registerBeanGonatsMsgPool() {
	basedi.RegisterLoadBean(singleNameGonatsMsgPool, LoadGonatsMsgPool)
}

func FindBeanGonatsMsgPool() *GonatsMsgPool {
	bean, ok := basedi.FindBean(singleNameGonatsMsgPool).(*GonatsMsgPool)
	if !ok {
		goutils.Error("FindBeanGonatsMsgPool: failed to cast bean to *GonatsMsgPool")

		return NewGonatsMsgPool()
	}
	return bean

}

func LoadGonatsMsgPool() baseiface.ISingleton {
	var s = NewGonatsMsgPool()
	InjectGonatsMsgPool(s)
	return s

}

func InjectGonatsMsgPool(s *GonatsMsgPool) {

	logrus.Debug("inject")
}
