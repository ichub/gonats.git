package gomsgpool

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/gopool"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
)

type GonatsMsgPool struct {
	basedto.BaseEntitySingle
	*gopool.GeneralObjectPool[*gomsg.GonatsMsg]
}

func NewGonatsMsgPool() *GonatsMsgPool {
	return &GonatsMsgPool{
		GeneralObjectPool: gopool.NewGeneralObjectPool[*gomsg.GonatsMsg](),
	}
}
