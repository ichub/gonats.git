package gomsgpool

import (
	"context"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"github.com/sirupsen/logrus"

	"testing"
	"time"
)

func Test001_pool(t *testing.T) {
	var pool = FindBeanGonatsMsgPool()
	var msg, _ = pool.BorrowObject(context.Background())
	defer pool.ReturnObject(context.Background(), msg)
	time.Sleep(time.Millisecond)
	goutils.Info(msg)
	time.Sleep(time.Millisecond)
}

func Test002_pool(t *testing.T) {
	var pool = FindBeanGonatsMsgPool()
	var msg, err = pool.BorrowObject(context.Background())
	logrus.Error(err)
	defer pool.ReturnObject(context.Background(), msg)
	goutils.Info(msg)

}
