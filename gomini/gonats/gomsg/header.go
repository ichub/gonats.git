package gomsg

import (
	"errors"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/util/grand"
	"github.com/nats-io/nats.go"
	"strings"
	"sync/atomic"
	"time"
)

var atomicInt64 atomic.Int64

type Header struct {
	basedto.BaseEntity `json:"-"`

	Domain     string `json:"domain"`
	Topic      string `json:"topic"`
	Action     string `json:"action"`
	MsgId      string `json:"msg_id"`
	SeqId      int64  `json:"seq_id"`
	ObjectId   string `json:"object_id"`
	ObjectType string `json:"object_type"`
	MsgReq     bool   `json:"msg_req"`
}

func NewSeqId() int64 {
	return atomicInt64.Add(1)
}

func NewGonatsHeader() *Header {

	var header = &Header{}
	header.InitProxy(header)
	return header.init()
}
func DefaultHeader() *Header {
	return NewGonatsHeader()

}

func (h *Header) init() *Header {
	seqId := NewSeqId()

	h.Action = goconsts.ES_ACTION_QUERY

	h.Domain = goconsts.DomainGeneral
	h.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_BIZ
	h.MsgId = baseutils.Uuid()
	h.ObjectId = gconv.String(time.Now().UnixNano())
	h.SeqId = seqId
	h.ObjectType = goconsts.Topic_DOMAIN_GENEAL_SyncES_BIZ
	h.MsgReq = true
	return h
}

func (h *Header) shutdown() {
	h.init()
}

func (h *Header) From(msg *nats.Msg) {
	h.Topic = msg.Subject

	if topic := msg.Header.Get("topic"); topic != "" {
		h.Topic = topic
	}
	h.Domain = msg.Header.Get("domain")
	h.Action = msg.Header.Get("action")
	h.MsgId = msg.Header.Get("msg_id")
	h.SeqId = gconv.Int64(msg.Header.Get("seq_id"))
	h.ObjectId = msg.Header.Get("object_id")
	h.ObjectType = msg.Header.Get("object_type")
	h.MsgReq = msg.Header.Get("msg_req") == "true"
}

func (h *Header) To(msg *nats.Msg) {

	msg.Subject = h.Topic
	msg.Header.Add("domain", h.Domain)
	msg.Header.Add("topic", h.Topic)
	msg.Header.Add("action", h.Action)
	msg.Header.Add("msg_id", h.MsgId)
	msg.Header.Add("seq_id", gconv.String(h.SeqId))
	msg.Header.Add("object_id", h.ObjectId)
	msg.Header.Add("object_type", h.ObjectType)
	msg.Header.Add("msg_req", gconv.String(h.MsgReq))

}

func (h *Header) Check() error {
	if h.Topic == "" {
		return errors.New("no topic")
	}
	return nil
}

func (h *Header) ChooseChannel() int {

	return grand.N(0, 9)

}
func (h *Header) Topic2Pre() string {
	var topics = strings.Split(h.Topic, ".")
	return topics[0] + ".*"
}
