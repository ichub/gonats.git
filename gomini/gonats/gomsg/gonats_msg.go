package gomsg

import (
	"errors"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/gomini/mini/general/query/page"
	pageesdto "gitee.com/ichub/gomini/mini/general/query/pagees"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"sync"

	"github.com/nats-io/nats.go"
)

// 定义错误常量
var (
	ErrInvalidType = errors.New("invalid model type")
	ErrJsonEncode  = errors.New("JSON encoding error")
	ErrJsonDecode  = errors.New("JSON decoding error")
)
var lock sync.RWMutex

type GonatsMsg struct {
	basedto.BaseEntity `json:"-"`
	*Header            `di:"auto" json:"header"`
	Data               []byte `json:"data"`
	//只为显示 ，不参与编码解码
	Body interface{} `json:"body,omitempty"`
}

// NewGonatsMsg 创建一个新的GonatsMsg实例。
func NewGonatsMsg() *GonatsMsg {
	var msg = &GonatsMsg{
		Header: NewGonatsHeader(),
	}
	msg.Init()
	return msg
}

// DefaultMsg 创建一个默认的GonatsMsg实例。
func Default() *GonatsMsg {
	return NewGonatsMsg()
}

// ToGonatsMsg 将nats.Msg转换为GonatsMsg。
func ToGonatsMsg(msg *nats.Msg) *GonatsMsg {
	var gmsg = NewGonatsMsg()
	gmsg.From(msg)
	return gmsg
}

func (gnm *GonatsMsg) Init() {

	if gnm.Header == nil {
		lock.Lock()
		defer lock.Unlock()
		gnm.Header = NewGonatsHeader()
	}
	gnm.InitProxy(gnm)
	gnm.Header = gnm.Header.init()
	gnm.Body = nil
	gnm.Data = make([]byte, 0)
}

func (gnm *GonatsMsg) Shutdown() {
	gnm.Header.shutdown()
	gnm.Data = nil
	gnm.Body = ""
}

// To: 转换GonatsMsg为nats.Msg
func (gnm *GonatsMsg) To() *nats.Msg {
	var msg = &nats.Msg{
		Header: make(nats.Header),
		Data:   gnm.Data,
	}
	gnm.Header.To(msg)
	return msg
}

func (gnm *GonatsMsg) FromReq(req *GonatsMsg) *GonatsMsg {

	gnm.Header = &(*(req.Header))
	return gnm
}

// From 将nats.Msg转换信息填充到GonatsMsg。
func (gnm *GonatsMsg) From(msg *nats.Msg) *GonatsMsg {
	gnm.Data = msg.Data
	gnm.Header.From(msg)
	return gnm
}

// Check 检查消息头是否有效。
func (gnm *GonatsMsg) Check() error {
	return gnm.Header.Check()
}

// SetData 设置消息数据。
func (gnm *GonatsMsg) SetData(model interface{}) error {
	if str, ok := model.(string); ok {
		gnm.Data = []byte(str)
	} else {
		gnm.Data, _ = jsonutils.ToJsonBytes(model)
	}
	return nil
}

// ParseData 解析消息数据。
func (gnm *GonatsMsg) DecodeData(out interface{}) error {

	if err := jsonutils.FromJsonByte(gnm.Data, out); err != nil {
		return ErrJsonDecode
	}
	gnm.Body = out

	goutils.Debug(jsonutils.ToJsonPretty(gnm))
	return nil
}

// 以下为示例方法，使用接口或工厂模式进行改进可能需要更多上下文信息。
func (gnm *GonatsMsg) Parse2PageRequest() {
	// 假设page.NewIchubPageRequest()是安全的。
	gnm.DecodeData(page.NewIchubPageRequest())
}

func (gnm *GonatsMsg) Parse2PageEsRequest() {
	// 假设pageesdto.NewPageesRequest()是安全的。
	gnm.DecodeData(pageesdto.NewPageesRequest())
}

func (gnm *GonatsMsg) IsMsgReq() bool {
	return gnm.Header.MsgReq

}

func (gnm *GonatsMsg) Decode() error {
	if gnm.IsMsgReq() && gnm.Header.Action == goconsts.ES_ACTION_QUERY {
		gnm.Parse2PageEsRequest()
	}
	if gnm.IsMsgReq() && gnm.Header.Action == goconsts.ES_ACTION_STAT {
		gnm.Parse2PageEsRequest()
	}

	return nil
}
func (gnm *GonatsMsg) Encode() *GonatsMsg {
	gnm.Data, _ = jsonutils.ToJsonBytes(gnm.Body)
	return gnm
}
