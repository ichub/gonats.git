package gomsg

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: header_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameHeader = "gomsg.Header"

// init register load
func init() {
	registerBeanHeader()
}

// register Header
func registerBeanHeader() {
	basedi.RegisterLoadBean(singleNameHeader, LoadHeader)
}

func FindBeanHeader() *Header {
	bean, ok :=  basedi.FindBean(singleNameHeader).(*Header)
    if !ok {
        logrus.Errorf("FindBeanHeader: failed to cast bean to *Header")
        return nil
    }
    return bean

}

func LoadHeader() baseiface.ISingleton {
	var s = NewGonatsHeader()
	InjectHeader(s)
	return s

}

func InjectHeader(s *Header)    {
    
    logrus.Debug("inject")
}


