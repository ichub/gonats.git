package gomsg

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gomini/mini/general/query/page"
	pageesdto "gitee.com/ichub/gomini/mini/general/query/pagees"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"github.com/nats-io/nats.go"
)

type GonatsMsgresp struct {
	basedto.BaseEntity `json:"-"`
	*GonatsMsg
}

func NewGonatsMsgresp() *GonatsMsgresp {
	var msg = &GonatsMsgresp{
		GonatsMsg: NewGonatsMsg(),
	}
	msg.InitProxy(msg)
	return msg
}

// NewResp 创建一个响应类型的GonatsMsg实例。
func NewRespMsg(req *GonatsMsg) *GonatsMsgresp {
	var msg = NewGonatsMsgresp()
	msg.FromReq(req)
	msg.MsgReq = false
	msg.InitProxy(msg)
	return msg
}

// NewGonatsMsgRes 创建一个从NATS消息响应的GonatsMsg实例。
func NewResp(req *nats.Msg) *GonatsMsgresp {
	var msg = NewGonatsMsgresp()
	msg.From(req)
	msg.MsgReq = false
	return msg
}

func EsNotSupport(msg *GonatsMsg) *GonatsMsgresp {
	var resp = NewRespMsg(msg)
	resp.Body = basedto.FailResult("msg not support!" + msg.Header.ToPrettyString())
	resp.Encode()
	return resp

}
func EsErrorResp(msg *GonatsMsg, err error) *GonatsMsgresp {
	var resp = NewRespMsg(msg)
	resp.Body = basedto.FailResult(err.Error())
	resp.Encode()
	return resp

}

func (self *GonatsMsgresp) Parse2PageEsResult() {
	// 假设pageesdto.NewPageesRequest()是安全的。
	self.DecodeData(pageesdto.NewPageesResult())
}

func (self *GonatsMsgresp) Parse2PageResult() {
	// 假设page.NewIchubPageResult()是安全的。
	self.DecodeData(page.NewIchubPageResult())
}

func (self *GonatsMsgresp) Parse2Result() {

	self.DecodeData(basedto.NewIchubResult())
}
func (self *GonatsMsgresp) Decode() error {

	if !self.IsMsgReq() && self.Header.Action == goconsts.ES_ACTION_QUERY {
		self.Parse2PageEsResult()
	}
	if !self.IsMsgReq() && self.Header.Action == goconsts.ES_ACTION_STAT {
		self.Parse2Result()
	}
	return nil
}
