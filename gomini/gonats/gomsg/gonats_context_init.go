package gomsg

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gonats_context_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGonatsContext = "gomsg.GonatsContext"

// init register load
func init() {
	registerBeanGonatsContext()
}

// register GonatsContext
func registerBeanGonatsContext() {
	basedi.RegisterLoadBean(singleNameGonatsContext, LoadGonatsContext)
}

func FindBeanGonatsContext() *GonatsContext {
	bean, ok :=  basedi.FindBean(singleNameGonatsContext).(*GonatsContext)
    if !ok {
        logrus.Errorf("FindBeanGonatsContext: failed to cast bean to *GonatsContext")
        return nil
    }
    return bean

}

func LoadGonatsContext() baseiface.ISingleton {
	var s = NewGonatsContext()
	InjectGonatsContext(s)
	return s

}

func InjectGonatsContext(s *GonatsContext)    {
    
    logrus.Debug("inject")
}


