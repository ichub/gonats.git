package gomsg

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gonats_msgresp_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGonatsMsgresp = "gomsg.GonatsMsgresp"

// init register load
func init() {
	registerBeanGonatsMsgresp()
}

// register GonatsMsgresp
func registerBeanGonatsMsgresp() {
	basedi.RegisterLoadBean(singleNameGonatsMsgresp, LoadGonatsMsgresp)
}

func FindBeanGonatsMsgresp() *GonatsMsgresp {
	bean, ok :=  basedi.FindBean(singleNameGonatsMsgresp).(*GonatsMsgresp)
    if !ok {
        logrus.Errorf("FindBeanGonatsMsgresp: failed to cast bean to *GonatsMsgresp")
        return nil
    }
    return bean

}

func LoadGonatsMsgresp() baseiface.ISingleton {
	var s = NewGonatsMsgresp()
	InjectGonatsMsgresp(s)
	return s

}

func InjectGonatsMsgresp(s *GonatsMsgresp)    {
    
    logrus.Debug("inject")
}


