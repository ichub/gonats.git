package gomsg

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"github.com/nats-io/nats.go"
)

type GonatsContext struct {
	basedto.BaseEntity
	*nats.Msg

	*GonatsMsg
	*GonatsMsgresp
}

func NewGonatsContext() *GonatsContext {
	return &GonatsContext{}
}
