package gomsg

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: gonats_msg_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGonatsMsg = "gomsg.GonatsMsg"

// init register load
func init() {
	registerBeanGonatsMsg()
}

// register GonatsMsg
func registerBeanGonatsMsg() {
	basedi.RegisterLoadBean(singleNameGonatsMsg, LoadGonatsMsg)
}

func FindBeanGonatsMsg() *GonatsMsg {
	bean, ok :=  basedi.FindBean(singleNameGonatsMsg).(*GonatsMsg)
    if !ok {
        logrus.Errorf("FindBeanGonatsMsg: failed to cast bean to *GonatsMsg")
        return nil
    }
    return bean

}

func LoadGonatsMsg() baseiface.ISingleton {
	var s = NewGonatsMsg()
	InjectGonatsMsg(s)
	return s

}

func InjectGonatsMsg(s *GonatsMsg)    {
    
    logrus.Debug("inject")
}


