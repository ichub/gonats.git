package gomsg

import (
	"fmt"
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/ichublog"
	"gitee.com/ichub/godi/di/difactroy"
	pagees "gitee.com/ichub/gomini/mini/general/query/pagees"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"sync"
	"testing"
	"time"
)

type TestGonatsMsgSuite struct {
	suite.Suite
	DiFactroy *difactroy.DiFactroy
	rootdir   string
}

func (self *TestGonatsMsgSuite) SetupTest() {
	logrus.Info("difactry...")
	ichublog.InitLogrus()
	self.rootdir = fileutils.FindRootDir()
	self.DiFactroy = difactroy.NewDiFactroy()

}

func TestGonatsMsgSuites(t *testing.T) {
	suite.Run(t, new(TestGonatsMsgSuite))
}

func (self *TestGonatsMsgSuite) Test001_EqQueryMsg() {

	var msg = FindBeanGonatsMsg()
	msg.Body = self.makeReq()
	msg.Domain = goconsts.DomainCms
	msg.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS
	msg.Action = goconsts.ES_ACTION_QUERY
	goutils.Info(msg)
}

func (self *TestGonatsMsgSuite) Test002_GoMsgSimple() {

	var msg = FindBeanGonatsMsg()
	goutils.Info(msg) //goutils.Info(msg.Header)
}

func (self *TestGonatsMsgSuite) makeReq() *pagees.PageesRequest {
	var esRequest = pagees.New()
	esRequest.PageSize = 3
	esRequest.IndexName = "ichub_sys_dept"
	esRequest.Source = "dept_id,dept_name" //esRequest.EsTerm("dept_name", "kl")
	return esRequest
}

func (self *TestGonatsMsgSuite) Test003_EsQueryResult() {

	esQueryResult, err := self.makeReq().EsQueryResult()
	if err != nil {
		goutils.Error(err)
	}
	goutils.Info(esQueryResult.ToPrettyString(), self.makeReq())

}

// 指定结构体structName生成注册与注入代码
func (self *TestGonatsMsgSuite) Test006_MakeDiStru() {

	self.DiFactroy.Rootdir = fileutils.FindRootDirGoMod()
	self.DiFactroy.MakeDiStru("EventFrame")

}

// 生成所有, 继承baseentity的结果: 已经有不再生成！
func (self *TestGonatsMsgSuite) Test007_MakeDiAll() {

	self.DiFactroy.MakeDiAll()

}

func (self *TestGonatsMsgSuite) Test008_MakeDiForce() {

	self.DiFactroy.MakeDiAllForce(true)

}

func (self *TestGonatsMsgSuite) worker(id int, wg *sync.WaitGroup) {
	defer wg.Done() // 通知主goroutine这个协程已经完成

	fmt.Printf("Worker %d starting\n", id)
	time.Sleep(time.Second) // 模拟工作
	fmt.Printf("Worker %d done\n", id)
}

func (self *TestGonatsMsgSuite) Test009_cocurrent() {

	var wg sync.WaitGroup
	for i := 1; i <= 5; i++ {
		wg.Add(1) // 增加WaitGroup的计数
		go self.worker(i, &wg)
	}

	wg.Wait() // 等待所有协程完成
	goutils.Info("All workers completed")
}
