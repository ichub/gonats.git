package main

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/gomini/mini/general/query/page"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"

	"gitee.com/ichub/gonats/gomini/natsserver/async/cliasync"
	"gitee.com/ichub/gonats/gomini/natsserver/sync/clisync"
	"github.com/gogf/gf/util/gconv"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
)

const msgnumber = 1000

func TestMain(m *testing.M) {

	m.Run() //goleak.VerifyTestMain(m)
}
func gotest() {
	logrus.SetLevel(logrus.DebugLevel)

	var msg = gomsg.FindBeanGonatsMsg()
	msg.Header.ObjectId = gconv.String(baseutils.SnowflakeNextVal())

	msg.Header.Topic = goconsts.Topic_DOMAIN_GENEAL_AsyncES_BIZ
	var result = basedto.NewIchubResult()
	msg.SetData(result)
	msg.Header.MsgReq = true
	msg.Parse2Result()

	var err = cliasync.FindBeanCliAsync().Publish(msg)
	if err != nil {
		goutils.Error(err)
	}
	goutils.Info(msg.ToPrettyString())
}
func Test001_asyncCli_publish(t *testing.T) {
	//t.Parallel()

	func() {

		for i := 0; i < msgnumber; i++ {
			go gotest()
			var msg = gomsg.FindBeanGonatsMsg()
			msg.Header.ObjectId = gconv.String(baseutils.SnowflakeNextVal())

			msg.Header.Topic = goconsts.Topic_DOMAIN_GENEAL_AsyncES_BIZ
			var result = basedto.NewIchubResult()
			msg.SetData(result)
			msg.Header.MsgReq = true
			msg.Parse2Result()

			var err = cliasync.FindBeanCliAsync().Publish(msg)
			if err != nil {
				goutils.Error(err)
			}
			goutils.Info(msg.ToPrettyString())
		}
	}()
}
func Test002_asyncCli_publish(t *testing.T) {
	t.Parallel()
	go func() {
		for i := 0; i < msgnumber; i++ {

			var msg = gomsg.FindBeanGonatsMsg()
			msg.Header.ObjectId = gconv.String(baseutils.SnowflakeNextVal())

			msg.Header.Topic = goconsts.Topic_DOMAIN_GENEAL_ASyncES_CMS
			var result = basedto.NewIchubResult()
			msg.SetData(result)
			msg.Header.MsgReq = true
			msg.Parse2Result()

			var err = cliasync.FindBeanCliAsync().Publish(msg)
			if err != nil {
				goutils.Error(err)
			}
			goutils.Info(msg.ToPrettyString())
			goutils.Error(msg.ToPrettyString())
		}
	}()
}
func Test003_syncCliTopicARequest(t *testing.T) {
	t.Parallel()
	for i := 0; i < msgnumber; i++ {

		var msg = gomsg.FindBeanGonatsMsg()
		//var ii = 1 + 100000 + i
		msg.Header.ObjectId = gconv.String(baseutils.SnowflakeNextVal()) //"abc" + gconv.String(100000+ii)
		var result = basedto.NewIchubResult()
		msg.SetData(result)
		msg.Header.MsgReq = true
		var resp, err = clisync.FindBeanCliSync().Request(msg)
		if err != nil {
			goutils.Error(err)
			continue
		}

		goutils.Info("resp=", jsonutils.ToJsonPretty(resp))
		var msgres = gomsg.NewGonatsMsgResp()
		msgres.From(resp)
		msgres.Parse2Result()
		goutils.Info("result new=", msgres)

	}
}

func Test004_syncCliTopicBrequest(t *testing.T) {
	t.Parallel()

	for i := 0; i < msgnumber; i++ {

		var msg = gomsg.FindBeanGonatsMsg()

		msg.Header.ObjectId = "abc" + gconv.String(100000+i)
		msg.Data = []byte("abc" + gconv.String(i+1000))
		var result = page.NewIchubPageResult()
		msg.SetData(result)
		msg.SetData(pagereq)

		msg.Header.MsgReq = true
		msg.Header.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS

		var resp, err = clisync.FindBeanCliSync().Request(msg)
		if err != nil {
			logrus.Error(err)
			continue
		}
		goutils.Info("resp=", jsonutils.ToJsonPretty(resp))
		var msgres = gomsg.NewGonatsMsgResp()
		msgres.From(resp)
		msgres.DecodeData(page.NewIchubPageResult())
		goutils.Info("result new=", msgres)
		//assert.Equal(t, "abc"+gconv.String(i+1000), string(msgres.Data))
		assert.Equal(t, goconsts.DomainGeneral, msgres.Header.Domain)
	}
}

const pagereq = `
{
  "page_size": 2,
  "current": 1,
  "order_by": null,
  "fields": [
    {
      "field": "email",
      "op_type": "like",
      "values": [
        "leijmdas@163.com"
      ]
    },
    {
      "field": "dept_id",
      "op_type": "between",
      "values": [
        "553270041122963456",
        "553270041122963456"
      ]
    }
  ],
  "index_name": "ichub_sys_dept"
} `
