package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

type WorkerFunc func()

type WorkerPool struct {
	workers int
	jobs    chan WorkerFunc
	wg      sync.WaitGroup
}

func NewWorkerPool(workers int) *WorkerPool {
	return &WorkerPool{
		workers: workers,
		jobs:    make(chan WorkerFunc, workers),
	}
}

func (p *WorkerPool) Start() {
	for i := 0; i < p.workers; i++ {
		p.wg.Add(1)
		go p.worker(i)
	}
}

func (p *WorkerPool) worker(workerID int) {
	defer p.wg.Done()

	for job := range p.jobs {
		job()
		fmt.Printf("Worker %d finished job\n", workerID)
	}
}

func (p *WorkerPool) Stop() {
	close(p.jobs)
	p.wg.Wait()
}

func (p *WorkerPool) Schedule(job WorkerFunc) {
	p.jobs <- job
}

func main() {
	pool := NewWorkerPool(10)
	pool.Start()

	var count int32
	for i := 0; i < 100; i++ {
		pool.Schedule(func() {
			n := atomic.AddInt32(&count, 1)
			fmt.Printf("Running job %d\n", n)
			time.Sleep(time.Millisecond * 500)
		})
	}

	time.Sleep(time.Second * 1)
	//pool.Stop()

	fmt.Println("All jobs completed")
}
