package difactroy

import (
	"errors"
	"fmt"
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/goconfig/common/ichublog"

	"gitee.com/ichub/godi/di/difactroy"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"runtime"
	"sync"
	"testing"
	"time"
)

type TestDifactoySuite struct {
	suite.Suite
	DiFactroy *difactroy.DiFactroy
	rootdir   string
}

func (self *TestDifactoySuite) SetupTest() {
	logrus.Info("difactry...")
	ichublog.InitLogrus()
	self.rootdir = fileutils.FindRootDir()
	self.DiFactroy = difactroy.NewDiFactroy()

}

func TestDifactroySuites(t *testing.T) {
	suite.Run(t, new(TestDifactoySuite))
}
func CurrentFile() string {
	_, file, _, ok := runtime.Caller(1)
	if !ok {
		panic(errors.New("Can not get current file！"))
	}
	return file
}

// 指定一个文件生成
func (self *TestDifactoySuite) Test001_MakeDiFile() {

	self.DiFactroy.MakeDiFile("./metadata/db/metadb/metadata_records.go")
	logrus.Info(CurrentFile())

}

// 指定结构体structName生成注册与注入代码
func (self *TestDifactoySuite) Test002_MakeDiStru() {

	self.DiFactroy.Rootdir = fileutils.FindRootDirGoMod()
	self.DiFactroy.MakeDiStru("EventFrame")

}

// 生成所有, 继承baseentity的结果: 已经有不再生成！
func (self *TestDifactoySuite) Test003_MakeDiAll() {

	self.DiFactroy.MakeDiAll()

}

// 生成所有 继承baseentity的结果，强制执行
func (self *TestDifactoySuite) Test004_MakeDiForce() {

	self.DiFactroy.MakeDiAllForce(true)

}

// 生成一个结构体的注册文件
func (self *TestDifactoySuite) Test006_MakeDiDto() {
	//var dto = didto.FindBeanDiDto()
	//dto.PkgName = "dimeta"
	//dto.StructName = "DiDto"
	//self.DiFactroy.MakeDi(dto)
}

// 查找所有GO文件
func (self *TestDifactoySuite) Test007_FindAllFiles() {
	self.DiFactroy.FindGoFiles()

	logrus.Info("basedi=", jsonutils.ToJsonPretty(self.DiFactroy))
}

// AST分析所有文件
func (self *TestDifactoySuite) Test008_ParseAll() {

	self.DiFactroy.ParseAll()
	var cf, found = self.DiFactroy.FindSome("SingleEntity")
	if found {
		logrus.Info(cf.ToString())
	} else {
		logrus.Error("\r\n!found ", found)
	}
}
func worker(id int, wg *sync.WaitGroup) {
	defer wg.Done() // 通知主goroutine这个协程已经完成

	fmt.Printf("Worker %d starting\n", id)
	time.Sleep(time.Second) // 模拟工作
	fmt.Printf("Worker %d done\n", id)
}

func (self *TestDifactoySuite) Test009_cocurrent() {

	var wg sync.WaitGroup
	for i := 1; i <= 5; i++ {
		wg.Add(1) // 增加WaitGroup的计数
		go worker(i, &wg)
	}

	wg.Wait() // 等待所有协程完成
	goutils.Info("All workers completed")
}
