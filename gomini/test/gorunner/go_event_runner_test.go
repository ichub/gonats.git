package gorunner

import (
	"fmt"
	"gitee.com/ichub/goconfig/common/ichubcontext"
	"github.com/sirupsen/logrus"
	"sync"
	"testing"
)

func Test001_chn(t *testing.T) {
	FindBeanGoEventRunner().Send2Channel()

	FindBeanGoEventRunner().RecvFromChannel()

}

func Test002_chn(t *testing.T) {

	// 创建10个channel
	channels := make([]chan int, 10)

	// 初始化channel数组
	for i := range channels {
		channels[i] = make(chan int)
	}

	// 用于等待所有goroutine完成
	var wg sync.WaitGroup
	// 启动写入数据的goroutine
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(ch chan int, val int) {
			defer wg.Done()
			ch <- val
		}(channels[i], i)
	}

	// 启动读取数据的goroutine
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(ch chan int) {
			defer wg.Done()
			val := <-ch
			fmt.Printf("Received value: %d\n", val)
		}(channels[i])
	}

	// 等待所有goroutine完成
	wg.Wait()
}
func Test003_chn(t *testing.T) {
	var n = ichubcontext.IchubClient.GetNatsDto()
	logrus.Info("n:", n)
}
