package gorunner

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/goproxy"

	"github.com/gogf/gf/util/gconv"
	"github.com/nats-io/nats.go"
)

type GoEventRunner struct {
	basedto.BaseEntitySingle
	*goproxy.GonatsProxy
}

func NewGoeventFramework() *GoEventRunner {
	var e = &GoEventRunner{
		GonatsProxy: goproxy.NewGonatsProxy(),
	}
	e.InitChannels()
	e.InitProxy(e)
	return e
}

func (this *GoEventRunner) Send2Channel() {
	for i := 0; i < goconsts.CHANNEL_NUMBER; i++ {
		this.WaitGroup.Add(1)
		go this.sendOne(this.Channels[i], i)
	}
	this.WaitGroup.Wait()
}
func (this *GoEventRunner) RecvFromChannel() {

	for i := 0; i < goconsts.CHANNEL_NUMBER; i++ {
		this.WaitGroup.Add(1)
		go this.recOne(this.Channels[i])
	}
	this.WaitGroup.Wait()
}
func (this *GoEventRunner) sendOne(ch chan *nats.Msg, i int) {
	defer this.WaitGroup.Done()
	var s = nats.NewMsg(gconv.String(i))
	goutils.Debug("send=", s)
	ch <- s
}

func (this *GoEventRunner) recOne(ch chan *nats.Msg) {
	defer this.WaitGroup.Done()
	var s *nats.Msg
	s = <-ch
	goutils.Debug("recv=", s)
}
