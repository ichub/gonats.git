package gorunner

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_event_runner_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGoEventRunner = "gorunner.GoEventRunner"

// init register load
func init() {
	registerBeanGoEventRunner()
}

// register GoEventRunner
func registerBeanGoEventRunner() {
	basedi.RegisterLoadBean(singleNameGoEventRunner, LoadGoEventRunner)
}

func FindBeanGoEventRunner() *GoEventRunner {
	bean, ok :=  basedi.FindBean(singleNameGoEventRunner).(*GoEventRunner)
    if !ok {
        logrus.Errorf("FindBeanGoEventRunner: failed to cast bean to *GoEventRunner")
        return nil
    }
    return bean

}

func LoadGoEventRunner() baseiface.ISingleton {
	var s = NewGoeventFramework()
	InjectGoEventRunner(s)
	return s

}

func InjectGoEventRunner(s *GoEventRunner)    {
    
    logrus.Debug("inject")
}


