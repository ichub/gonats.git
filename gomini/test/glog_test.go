package test

import (
	"context"
	"gitee.com/ichub/goconfig/common/ichubcontext"

	"testing"
)

func Test001_glog(t *testing.T) {
	logger := goutils.NewGoLog()
	logger.Info(context.Background(), ichubcontext.IchubClient.IniMysqlClient().IchubConfig())
}
func Test002_glog(t *testing.T) {
	logger := goutils.NewGoLog()
	logger.Info(context.Background(), ichubcontext.IchubClient.IniMysqlClient().IchubConfig())
	//var cfg = gutil.Copy(ichubcontext.IchubClient.IniMysqlClient().IchubConfig())
	//logger.Error(context.Background(), cfg)
}
