package test

import (
	"context"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/goconfig/common/ichubcontext"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"github.com/sirupsen/logrus"
	"testing"

	"github.com/gogf/gf/v2/os/gcfg"
)

//https://www.bookstack.cn/read/goframe-2.4-zh/5c6d5b1ad9ad8324.md

// https://mp.csdn.net/mp_blog/creation/editor/138333915

func Test001_gcfg(t *testing.T) {

	if ad, err := gcfg.NewAdapterFile("../config/app.yml"); err == nil {
		var r, e = ad.Get(context.Background(), "etcd.server")
		logrus.Info("r=", jsonutils.ToJsonPretty(r), e)
	} else {
		logrus.Error(err)
	}

}
func Test002_gcfgRedis(t *testing.T) {
	ichubcontext.IchubClient.IniRedisClient()
	var dto = ichubcontext.IchubClient.GetNatsDto()
	logrus.Info("redis=", ichubcontext.IchubClient.RedisClient())
	logrus.Info("dto=", dto)

}

func Test003_LoadBean(t *testing.T) {
	ichubcontext.IchubClient.IniRedisClient()
	var dto = ichubcontext.IchubClient.GetNatsDto()
	logrus.Info("redis=", ichubcontext.IchubClient.RedisClient())
	logrus.Info("dto=", dto)
	var msg = gomsg.FindBeanGonatsMsg()
	logrus.Info(msg.ToPrettyString())

}
