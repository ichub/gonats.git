package eventcontainer

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_event_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGoEvent = "eventcontainer.GoEvent"

// init register load
func init() {
	registerBeanGoEvent()
}

// register GoEvent
func registerBeanGoEvent() {
	basedi.RegisterLoadBean(singleNameGoEvent, LoadGoEvent)
}

func FindBeanGoEvent() *GoEvent {
	bean, ok :=  basedi.FindBean(singleNameGoEvent).(*GoEvent)
    if !ok {
        logrus.Errorf("FindBeanGoEvent: failed to cast bean to *GoEvent")
        return nil
    }
    return bean

}

func LoadGoEvent() baseiface.ISingleton {
	var s = NewGoEvent()
	InjectGoEvent(s)
	return s

}

func InjectGoEvent(s *GoEvent)    {
    
    logrus.Debug("inject")
}


