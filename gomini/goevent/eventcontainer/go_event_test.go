package eventcontainer


import (
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goconfig/common/ichublog"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

type TestGoEventSuite struct {
	suite.Suite
	rootdir string
}

func (this *TestGoEventSuite) SetupTest() {

	ichublog.InitLogrus()
	this.rootdir = fileutils.FindRootDir()

}

func TestGoEventSuites(t *testing.T) {
	suite.Run(t, new(TestGoEventSuite))
}

// 指定一个文件生成
func (this *TestGoEventSuite) Test001_func() {
    logrus.Info(1)

}


