package eventcontainer

const (
	GOEvent_Type_NATS = "NATS"
	GOEvent_Type_ES   = "ES"
	GOEvent_Type_WEB  = "WEB"

	GOEvent_Type_GOMSG = "GoMsg"
)
