package eventcontainer

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gonats/gomini/goevent/eventiface"
)

type GoEvent struct {
	basedto.BaseEntity

	//事件id
	EventId string `json:"event_id"`
	//事件名称
	EventName string `json:"event_name"`
	//事件类型：可以是聚合根名称
	ObjectType string `json:"object_type"`
	//事件处理器
	Handler eventiface.IEventHandler `json:"-"`
}

func NewGoEvent() *GoEvent {
	var event = &GoEvent{}
	event.InitProxy(event)
	return event
}

func (this *GoEvent) Key() string {
	return this.ObjectType + "::" + this.EventId
}
