package eventcontainer

import (
	"gitee.com/ichub/gonats/gomini/goevent/eventiface"
	"github.com/sirupsen/logrus"
	"sync"
)

func FindHandlerNats(eventid string) eventiface.IEventHandler {
	return container.FindHandler(eventid, GOEvent_Type_NATS)
}

func RegisterHandlerES(eventid string, handler eventiface.IEventHandler) {
	RegisterHandler(eventid, GOEvent_Type_ES, handler)
}
func RegisterHandlerNats(eventid string, handler eventiface.IEventHandler) {
	RegisterHandler(eventid, GOEvent_Type_NATS, handler)
}
func RegisterHandler(eventid string, ObjectType string, handler eventiface.IEventHandler) {
	container.RegisterHandler(eventid, ObjectType, handler)
}
func UnRegisterHandler(eventid string, ObjectType string) {
	container.UnRegisterHandler(eventid, ObjectType)
}
func FindHandler(eventid string, ObjectType string) eventiface.IEventHandler {
	return container.FindHandler(eventid, ObjectType)
}

// EventContainer is a global container for gohandler handlers.
var container = NewEventContainer()

type EventContainer struct {
	container map[string]*GoEvent
	lock      sync.RWMutex
}

func NewEventContainer() *EventContainer {
	return &EventContainer{
		container: make(map[string]*GoEvent),
	}
}

func (self *EventContainer) UnRegisterHandler(eventId string, ObjectType string) {
	self.lock.Lock()
	defer self.lock.Unlock()

	var event = NewGoEvent()
	event.EventId = eventId
	event.ObjectType = ObjectType
	delete(self.container, event.Key())

}
func (self *EventContainer) RegisterHandler(eventId string, ObjectType string, handler eventiface.IEventHandler) {
	self.lock.Lock()
	defer self.lock.Unlock()

	var event = NewGoEvent()
	event.EventId = eventId
	event.Handler = handler
	event.ObjectType = ObjectType
	self.container[event.Key()] = event
	logrus.Info("register ", eventId)
}

func (self *EventContainer) FindHandler(eventid string, ObjectType string) eventiface.IEventHandler {
	self.lock.RLock()
	defer self.lock.RUnlock()

	var event = NewGoEvent()
	event.EventId = eventid
	event.ObjectType = ObjectType
	var e, ok = self.container[event.Key()]
	if ok {
		return e.Handler
	}
	return nil
}
