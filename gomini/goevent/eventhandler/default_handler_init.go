package eventhandler

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: default_handler_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameDefaultHandler = "eventhandler.DefaultHandler"

// init register load
func init() {
	registerBeanDefaultHandler()
}

// register DefaultHandler
func registerBeanDefaultHandler() {
	basedi.RegisterLoadBean(singleNameDefaultHandler, LoadDefaultHandler)
}

func FindBeanDefaultHandler() *DefaultHandler {
	bean, ok :=  basedi.FindBean(singleNameDefaultHandler).(*DefaultHandler)
    if !ok {
        logrus.Errorf("FindBeanDefaultHandler: failed to cast bean to *DefaultHandler")
        return nil
    }
    return bean

}

func LoadDefaultHandler() baseiface.ISingleton {
	var s = NewDefaultHandler()
	InjectDefaultHandler(s)
	return s

}

func InjectDefaultHandler(s *DefaultHandler)    {
    
    logrus.Debug("inject")
}


