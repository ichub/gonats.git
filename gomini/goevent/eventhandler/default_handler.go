package eventhandler

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gonats/gomini/goevent/eventiface"
)

type DefaultHandler struct {
	basedto.BaseEntitySingle
}

func NewDefaultHandler() *DefaultHandler {
	return &DefaultHandler{}
}

// https://cloud.tencent.com/developer/article/2244995
func (this *DefaultHandler) Execute(context *eventiface.EventContext) {

}
