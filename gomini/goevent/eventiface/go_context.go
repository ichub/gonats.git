package eventiface

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
)

type GoContext struct {
	//GetContext()   interface{}
	basedto.BaseEntity
}

func NewGoContext() *GoContext {
	return &GoContext{}
}

func (self *GoContext) FindGonatsMsg() *gomsg.GonatsMsg {

	return gomsg.Default()
}
func (self *GoContext) Execute() *gomsg.GonatsMsg {
	goutils.Info("?Exe...")
	return gomsg.Default()
}
