package eventiface

import "gitee.com/ichub/gonats/gomini/gonats/gomsg"

type EventContext interface {
	Execute() *gomsg.GonatsMsg
	FindGonatsMsg() *gomsg.GonatsMsg
}
