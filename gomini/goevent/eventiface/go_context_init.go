package eventiface

import (
	"gitee.com/ichub/goconfig/common/base/baseiface"
	"gitee.com/ichub/goconfig/common/basedi"
    "github.com/sirupsen/logrus"
	
)

/*
 * ********************************************************
 * godi工具生成代码，不建议直接修改!
 * ********************************************************
 */

/* ********************************************************
	@Title  文件名称: go_context_init.go
	@Desp   描述:    自动注册注入

	@Author 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)
	@Update 作者: leijmdas@163.com 时间(2024-05-14 07:57:21)

* *********************************************************/

const singleNameGoContext = "eventiface.GoContext"

// init register load
func init() {
	registerBeanGoContext()
}

// register GoContext
func registerBeanGoContext() {
	basedi.RegisterLoadBean(singleNameGoContext, LoadGoContext)
}

func FindBeanGoContext() *GoContext {
	bean, ok :=  basedi.FindBean(singleNameGoContext).(*GoContext)
    if !ok {
        logrus.Errorf("FindBeanGoContext: failed to cast bean to *GoContext")
        return nil
    }
    return bean

}

func LoadGoContext() baseiface.ISingleton {
	var s = NewGoContext()
	InjectGoContext(s)
	return s

}

func InjectGoContext(s *GoContext)    {
    
    logrus.Debug("inject")
}


