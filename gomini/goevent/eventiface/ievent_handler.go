package eventiface

type IEventHandler interface {
	Execute(context EventContext)
}
