package goperfstat

import (
	"errors"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"strings"
	"sync"
	"time"
)

const STAT_KEY_ALL = "TOPIC_ALL"
const STAT_LOG_INTERVAL time.Duration = 2 //* time.Second

// 未来增加统计条件

var ticker *time.Ticker // time.NewTicker(time.Second * STAT_LOG_INTERVAL)
var once sync.Once

type GoperfStat struct {
	basedto.BaseEntitySingle

	StatMap map[string]*GostatInfo
	start   time.Time
	channel chan *GostatInfo
	Lock    sync.RWMutex
}

func TimerLog() {
	for {
		select {
		case <-ticker.C:
			FindBeanGoperfStat().safeStatLog()
		}
	}
}
func NewGoperfStat() *GoperfStat {
	var s = &GoperfStat{
		StatMap: make(map[string]*GostatInfo),
		start:   time.Now(),
		channel: make(chan *GostatInfo, 100),
	}
	s.StatMap[STAT_KEY_ALL] = FindBeanGostatInfo()
	s.InitProxy(s)
	return s
}

func (self *GoperfStat) StartStats() {
	once.Do(func() {
		ticker = time.NewTicker(time.Second * STAT_LOG_INTERVAL)
		go TimerLog()
	})
	go func() {
		for {
			for g := range self.channel {
				self.Stat(g)
			}
			time.Sleep(time.Millisecond)
		}
	}()
}
func (self *GoperfStat) Trigger(topic string, ok bool) {
	var gostatInfo = FindBeanGostatInfo()
	gostatInfo.Topic = topic
	if ok {
		gostatInfo.Oknum = 1
	} else {
		gostatInfo.Failnum = 1
	}
	self.channel <- gostatInfo
}

func (self *GoperfStat) TopicType(topic string) (string, error) {
	if topic == "" {
		return "", errors.New("invalid topic: empty!")
	}
	ss := strings.Split(topic, ".")
	if len(ss) < 2 {
		return "", errors.New("invalid topic format!")
	}
	return ss[0], nil
}

func (self *GoperfStat) Stat(g *GostatInfo) error {
	if g.Topic == goconsts.TopicGeneralSyncSTAT {
		return nil
	}
	self.Lock.Lock()
	defer self.Lock.Unlock()
	var topic = g.Topic
	topicType, err := self.TopicType(topic)
	if err != nil {
		goutils.Error(err)
		return err
	}
	// Use LoadOrStore for atomic insert or update
	if _, ok := self.StatMap[topic]; !ok {
		self.StatMap[topic] = FindBeanGostatInfo()
	}
	if _, ok := self.StatMap[topicType]; !ok {
		self.StatMap[topicType] = FindBeanGostatInfo()
	}

	self.StatMap[topic].Stat(topic, g.Oknum == 1)
	self.StatMap[topicType].Stat(topicType, g.Oknum == 1)
	self.StatMap[STAT_KEY_ALL].Stat(STAT_KEY_ALL, g.Oknum == 1)
	return nil
}

func (self *GoperfStat) safeStatLog() {

	self.Lock.RLock()
	defer self.Lock.RUnlock()
	for k, v := range self.StatMap {
		if v.change {
			v.change = false
			goutils.Stat("performstat: ", k, "=>", v)
		}
	}

}
func (self *GoperfStat) FindStatMap() map[string]*GostatInfo {
	self.Lock.RLock()
	defer self.Lock.RUnlock()
	var mapStat = make(map[string]*GostatInfo)
	for k, v := range self.StatMap {
		mapStat[k] = v
	}
	return mapStat
}
