package goperfstat

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
)

const STAT_MSG_TYPE_NATS = "nats"

type GostatInfo struct {
	basedto.BaseEntity

	Domain   string `json:"domain"`
	MsgType  string `json:"msgtype"`
	Topic    string `json:"topic"`
	Oknum    int64  `json:"oknum"`
	Failnum  int64  `json:"failnum"`
	Totalnum int64  `json:"totalnum"`

	change bool `json:"change"`
}

func NewGostatInfo() *GostatInfo {
	return &GostatInfo{

		MsgType: STAT_MSG_TYPE_NATS,
		Domain:  goconsts.DomainGeneral,

		change: false,
	}
}

func (self *GostatInfo) Stat(topic string, ok bool) {
	self.Topic = topic
	if !ok {
		self.Failnum += 1
	} else {
		self.Oknum += 1
	}
	self.Totalnum += 1
	self.change = true
}
