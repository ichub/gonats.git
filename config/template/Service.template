package esservice


/*
   @Title  文件名称 : {{.FileName}}
   @Description 描述: {{.Description}}

   @Author  作者: {{.Author}}  时间({{.DATETIME}})
   @Update  作者: {{.Author}}  时间({{.DATETIME}})

*/

import (
    "fmt"
    basedto "icd/hub/base/dto"
    "icd/hub/shop/dao"
    "icd/hub/shop/dto"
    "icd/hub/shop/model"
    "icd/hub/shop/inter"
)

// SERVICE层服务结构体
type {{.ESModelName}}Service struct {
  Inst{{.ModelName}}DAO   dao.{{.ModelName}}DAO
}

// SERVICE层服务实例变量
var Inst{{.ESModelName}}Service inter.{{.ModelName}}Interface=new ( {{.ESModelName}}Service )

/*
   @title     函数名称:  Query
   @description      :  通用查询
   @auth      作者:      {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: param * dto.{{.ModelName}}QueryParam
   @return    返回参数名: * dto.{{.ModelName}}PageResult
*/
func (serviceInst * {{.ESModelName}}Service) Query(param *dto.{{.ModelName}}QueryParam)  *dto.{{.ModelName}}PageResult {

	entities , err := serviceInst.Inst{{.ModelName}}DAO.FindByQueryParam(param)
	var result dto.{{.ModelName}}PageResult
	if err != nil {
		result.FailMessage(err.Error())
		return &result
	}
	result.Page.Count = 0
	if entities != nil {
		result.Page.Count = len(*entities)
		serviceInst.fills(entities)
	}
	result.Success()
	result.Data = *entities
	result.Page.Total, _ = serviceInst.Inst{{.ModelName}}DAO.CountByQueryParam(param)
    result.Page.Current = param.Current
    result.Page.PageSize = param.PageSize

	return &result
}

/*
   @title     函数名称 :  Count
   @description       :  通用查询计数
   @auth      作者     : {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名: param * dto.{{.ModelName}}QueryParam
   @return    返回参数名: * dto.{{.ModelName}}JsonResult
*/
func (serviceInst *{{.ESModelName}}Service) Count(param *dto.{{.ModelName}}QueryParam)   ( result * basedto.JsonResult ) {
    result = &basedto.JsonResult{}
	result.Success()

	cnt , err := serviceInst.Inst{{.ModelName}}DAO.CountByQueryParam(param)
	if err != nil {
        result.FailMessage(err.Error())
        return result
    }

    key  := fmt.Sprintf("%d",cnt)
   	result.Data =  key

   	return result
}


/*

 @title     函数名称: fills
 @description      : 填充关联子表信息。
 @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名: entity *model.{{.ModelName}}
 @return    返回参数名: 无

*/
func (serviceInst *{{.ESModelName}}Service) fills(entities *[]model.{{.ModelName}}) {

	for index := range *entities {

		serviceInst.fill(&(*entities)[index])
	}

}

/*
   @title     函数名称: FindById({{.pkey}} {{.pkeyType}})
   @description      : 根据主键查询记录
   @auth      作者:     {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名:{{.pkey}} {{.pkeyType}}
   @return    返回参数名:*dto.{{.ModelName}}JsonResult
*/
func (serviceInst *{{.ESModelName}}Service) FindById({{.pkey}} {{.pkeyType}}) *dto.{{.ModelName}}JsonResult {

    var result dto.{{.ModelName}}JsonResult
    entity, found, err :=  serviceInst.Inst{{.ModelName}}DAO.FindById({{.pkey}})

    if err != nil {
   		result.FailCodeMsg( basedto.CODE_NOFOUND_RECORD,err.Error())
		return &result
	}
  	if !found {
        result.Success()      	//result.FailCodeMsg( basedto.CODE_NOFOUND_RECORD,err.Error())
		return &result
	}

    if entity != nil &&  found {
        serviceInst.fill(entity)
        result.Success()
        result.Data=entity
    }else{
        result.SuccessMessage("没有找到记录！" )
    }


    return &result

}

/*
 @title     函数名称: fill
 @description      : 填充关联子表信息。
 @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名: entity *model.{{.ModelName}}
 @return    返回参数名: 无
*/
func (serviceInst *{{.ESModelName}}Service)  fill(entity *model.{{.ModelName}}) {



}

/*
   @title     函数名称: FindByIds(pks string)
   @description      :
              根据主键{{.pkey}}  查询多条记录
              例子： FindByIds("1,36,39")
   @auth      作者:     {{.Author}} 时间: {{.DATETIME}}
   @param     输入参数名:{{.pkey}} {{.pkeyType}}
   @return    返回参数名:*dto.{{.ModelName}}JsonResult
*/
func (serviceInst *{{.ESModelName}}Service)  FindByIds(pks  string) *dto.{{.ModelName}}PageResult {
	var result dto.{{.ModelName}}PageResult
	entities, err := serviceInst.Inst{{.ModelName}}DAO.FindByIds(pks)
    if err != nil {
        result.FailMessage(err.Error())
        return  &result
    }

	result.SetData(*entities) 
	if entities != nil {
		result.Page.Count = len(*entities)
	    result.Page.Total = int32(len(*entities))
		serviceInst.fills(entities)
	}
	result.Code = 200
    result.Msg = "成功"

	return &result
}

/*

 @title     函数名称: DeleteById
 @description      : 根据主键软删除。
 @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名: {{.pkey}} {{.pkeyType}}
 @return    返回参数名: *basedto.JsonResult

*/
func (serviceInst *{{.ESModelName}}Service)  DeleteById ({{.pkey}} {{.pkeyType}}) *basedto.JsonResult {
    err := serviceInst.Inst{{.ModelName}}DAO.DeleteById({{.pkey}})
	var result basedto.JsonResult
	if err != nil {
		result.FailMessage(err.Error())
		return  &result
	}
	result.Success()
	return &result

}


/*

 @title     函数名称: Save
 @description      : 主键%s为nil,0新增; !=nil修改。
 @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名: entity *model.{{.ModelName}}
 @return    返回参数名: *basedto.JsonResult
*/
func (serviceInst *{{.ESModelName}}Service) Save(entity *model.{{.ModelName}}) *basedto.JsonResult {
 
	var e error
	if  entity.{{.pkey}} == {{.PkeyValue}} {
		entity.{{.pkey}} = 0
		_ , e = serviceInst.Inst{{.ModelName}}DAO.Insert(entity)

	} else {
		_ , e = serviceInst.Inst{{.ModelName}}DAO.Update(entity)
	}
	var result basedto.JsonResult
	if e != nil {
		result.FailMessage(e.Error())
		return  &result
	}

	result.SuccessData(fmt.Sprintf("{{.fmtType}}",entity.{{.pkey}}))
	return &result

}


/*

 @title     函数名称: UpdateNotNullProps
 @description      : 更新非空字段
 @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名: entity *model.{{.ModelName}}
 @return    返回参数名: *basedto.JsonResult

*/
func (serviceInst *{{.ESModelName}}Service)  UpdateNotNullProps(Id int64,entity map[string]interface{}) *basedto.JsonResult {
	var result basedto.JsonResult
	_,err := serviceInst.Inst{{.ModelName}}DAO.UpdateNotNullProps(Id,entity)

	if err != nil {
		result.FailMessage(err.Error())
		return  &result
	}
	return result.Success()
}

/*
 @title     函数名称: Demo
 @description      : 代码示例
 @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
 @param     输入参数名: 无
 @return    返回参数名: 无

*/
func (serviceInst *{{.ESModelName}}Service) Demo() {
    var entity model.{{.ModelName}}

    entity.Ini(false)
	result := serviceInst.Save(&entity)
    fmt.Println( result )

    findResult := serviceInst.FindById(entity.{{.pkey}})
    fmt.Println(findResult)


}


// End of  {{.ESModelName}}Service