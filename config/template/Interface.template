package inter

/*
   @Title  文件名称 : {{.FileName}}
   @Description 描述: {{.Description}}

   @Author  作者: {{.Author}}  时间({{.DATETIME}})
   @Update  作者: {{.Author}}  时间({{.DATETIME}})

*/

import (
    basedto "icd/hub/base/dto"
    "icd/cms/shop/dto"
    "icd/cms/shop/model"
)


type {{.ModelName}}Interface interface {

    /*
       @title     函数名称:  Query
       @description      :  通用查询
       @auth      作者   :  {{.Author}} 时间: {{.DATETIME}}
       @param     输入参数名: param * dto.{{.ModelName}}QueryParam
       @return    返回参数名: * dto.{{.ModelName}}PageResult
    */
    Query(queryParam *dto.{{.ModelName}}QueryParam)  *dto.{{.ModelName}}PageResult

    /*
       @title     函数名称 :  Count
       @description       :  通用查询计数
       @auth      作者     : {{.Author}} 时间: {{.DATETIME}}
       @param     输入参数名: param * dto.{{.ModelName}}QueryParam
       @return    返回参数名: * dto.{{.ModelName}}JsonResult
    */
    Count(queryParam *dto.{{.ModelName}}QueryParam)  *basedto.JsonResult

    /*
       @title     函数名称: FindById({{.pkey}} {{.pkeyType}})
       @description      : 根据主键查询记录
       @auth      作者:     {{.Author}} 时间: {{.DATETIME}}
       @param     输入参数名:{{.pkey}} {{.pkeyType}}
       @return    返回参数名:*dto.{{.ModelName}}JsonResult
    */
    FindById({{.pkey}} {{.pkeyType}}) *dto.{{.ModelName}}JsonResult



    /*
       @title     函数名称: FindByIds(pks string)
       @description      :
                  根据主键{{.pkey}} 查询多条记录  例子： FindByIds("1,36,39")
       @auth      作者:     {{.Author}} 时间: {{.DATETIME}}
       @param     输入参数名:{{.pkey}} {{.pkeyType}}
       @return    返回参数名:*dto.{{.ModelName}}JsonResult
    */
    FindByIds(pks  string) *dto.{{.ModelName}}PageResult

    /*

         @title     函数名称: DeleteById
         @description      : 根据主键软删除。
         @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
         @param     输入参数名: {{.pkey}} {{.pkeyType}}
         @return    返回参数名: *basedto.JsonResult

    */
    DeleteById({{.pkey}} {{.pkeyType}}) *basedto.JsonResult


    /*

         @title     函数名称: Save
         @description      : 主键%s为nil,0新增; !=nil修改。
         @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
         @param     输入参数名: entity *model.{{.ModelName}}
         @return    返回参数名: *basedto.JsonResult
    */
    Save(entity *model.{{.ModelName}}) *basedto.JsonResult



    /*

         @title     函数名称: UpdateNotNullProps
         @description      : 更新非空字段
         @auth      作者   : {{.Author}} 时间: {{.DATETIME}}
         @param     输入参数名: Id int64,entity map[string]interface{}
         @return    返回参数名: *basedto.JsonResult

    */
    UpdateNotNullProps(Id int64,entity map[string]interface{}) *basedto.JsonResult
}