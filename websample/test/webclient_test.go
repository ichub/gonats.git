package test

import (
	"fmt"
	"reflect"

	"gitee.com/ichub/goweb/common/webclient"
	"github.com/gookit/goutil/strutil"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"testing"
)

/*
	@Title    文件名称: asuite_test.go
	@Description  描述: 有芯规则引擎执行的函数与对象集

	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
// 依赖 suite.Suite
type TestWebSuite struct {
	suite.Suite
	webClient *webclient.IchubWebClient
}

// 用于 'go test' 的入口
func TestWebSuites(t *testing.T) {
	suite.Run(t, new(TestWebSuite))
}

// 每个测试运行前，会执行
func (suite *TestWebSuite) SetupTest() {

	logrus.Info("SetupTest")
	//var config = ichubconfig.New(ichubconfig.ConfigfileApp)
	//clientDto := config.ReadIchubWebClient()

	suite.webClient = webclient.Default()

}

func (suite *TestWebSuite) TearDownTest() {

	suite.T().Log("TearDownTest")
}
func (suite *TestWebSuite) SetupSuite() {
	fmt.Println("Suite setup", "领域驱动设计，测试驱动开发!")

}

func (suite *TestWebSuite) TearDownSuite() {
	fmt.Println("Suite teardown")
}

func (suite *TestWebSuite) Check(exp any, real any, msg ...string) {
	fmt.Println("real=", real)
	expect, _ := strutil.ToString(exp)
	r, _ := strutil.ToString(real)
	suite.Equal(expect, r, msg)
}

func (suite *TestWebSuite) Test001_getWorld() {

	var result = suite.webClient.Get("/")
	logrus.Info(result)

}
func (suite *TestWebSuite) Test002_postWorld() {

	var result = suite.webClient.Post("{}", "/")
	logrus.Info(result)

	tmpStuct := []reflect.StructField{
		{
			Name: "Height",
			Type: reflect.TypeOf(float64(0)),
			Tag:  `json:"height"`,
		},
		{
			Name: "Age",
			Type: reflect.TypeOf(0),
			Tag:  `json:"age"`,
		},
		{
			Name: "Test",
			Type: reflect.TypeOf(0),
			Tag:  `json:"test"`,
		},
	}
	typ := reflect.StructOf(tmpStuct)
	fmt.Printf("%v", typ)
	structValue := reflect.New(typ).Elem()
	// 设置字段的值
	structValue.FieldByName("Test").SetInt(0)
	structValue.FieldByName("Age").SetInt(30)
	fmt.Println(structValue)

}
