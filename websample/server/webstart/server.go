package webstart

import (
	"gitee.com/ichub/goconfig/common/ichubconfig"
	"gitee.com/ichub/goconfig/common/ichublog"
	"gitee.com/ichub/goweb/common/webserver"
	"gitee.com/ichub/gonats/websample/server/router"
	"github.com/sirupsen/logrus"
)

/*
	@Title    文件名称: server.go
	@Description  描述: 通用引擎微服务
	@Contact.user raymond
	@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
	@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/

// https://www.jianshu.com/p/982c4fabb11d swagg参数

func StartWeb() {
	defer func() {
		if r := recover(); r != nil {
			ichublog.Error("[main] Recovered in ", r)

		}
	}()

	var config = ichubconfig.New(ichubconfig.ConfigfileApp)
	serverDto := config.ReadIchubWebServer()
	swaggerClientDto := config.ReadWebSwagger()

	ichublog.Log("serverDto=", serverDto)
	var server = webserver.New(serverDto)
	logrus.Info("http://localhost:88/swagger/index.html#/")
	//注册服务
	if swaggerClientDto.Enable == "false" {
		server.StartWeb(router.Register)
	} else {
		server.StartWebSwagger(router.Swagger, router.Register)

	}
}

// go get -u -v github.com/swaggo/gin-swagger//go get -u -v github.com/swaggo/files
// go get -u -v github.com/alecthomas/template
