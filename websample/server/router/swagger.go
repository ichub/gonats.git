package router

import (
	"gitee.com/ichub/goconfig/common/ichubconfig"
	"gitee.com/ichub/gonats/websample/server/docs"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func Swagger(router *gin.Engine) *gin.Engine {
	var ichubconfig = ichubconfig.NewIchubConfig()
	var swaggerClientDto = ichubconfig.ReadWebSwagger()

	logrus.Info(swaggerClientDto)
	docs.SwaggerInfo.BasePath = swaggerClientDto.BasePath
	docs.SwaggerInfo.Host = swaggerClientDto.Host
	docs.SwaggerInfo.Version = swaggerClientDto.Version //"v1.0.0"
	docs.SwaggerInfo.Title = swaggerClientDto.Title

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	return router
}
