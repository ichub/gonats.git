package webcommon

/*
@Title    文件名称: ichub_consts.go
@Description  描述: 有芯规则引擎常量

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
const (
	WEB_BASEPATH_CONFIG = "config"
	WEB_BASEPATH_FILE   = "file"
	WEB_BASEPATH_PUBLIC = "public"
)
const (
	FUNC_PARAM_EMPTY = "{}"

	FUNC_HELPAPI    = "helpapi"
	FUNC_AllAddr    = "alladdr"
	FUNC_AllService = "allservice"

	FUNC_RULEDATA       = "ruledata"
	FUNC_RULEFILE       = "rulefile"
	FUNC_POSTRULE       = "engine"
	FUNC_POSTRULE_BATCH = "ichubengineBatch"

	FUNC_LISTFUNCS      = "func_listfuncs"
	FUNC_LISTFUNC       = "func_listfunc"
	FUNC_SNOWFLAKE      = "func_snowflake"
	FUNC_HELLOWORLD     = "HelloWorld"
	FUNC_HELLOWORLDPOST = "HelloWorldPOST"

	FUNC_DEMO   = "func_demo"
	FUNC_CONFIG = "func_config"

	FUNC_ESQUERY      = "func_esquery"
	FUNC_ESGETMAPPING = "func_esgetmapping"

	FUNC_DBQUERY    = "func_dbquery"
	FUNC_DBMETADATA = "func_dbmetadata"
)
const (
	PATH_HelloWorld = "/"
	PATH_RuleData   = "/ruledata"
	PATH_RuleFile   = "/rulefile"
	PATH_HelpApi    = "/helpapi"

	PATH_AllAddr    = "/alladdr"
	PATH_AllService = "/allservice"

	PATH_Rule      = "/engine"
	PATH_RuleBatch = "/ichubengineBatch"

	PATH_LISTFUNCS = "/listfuncs"
	PATH_LISTFUNC  = "/listfunc"

	PATH_ESQUERY      = "/esquery"
	PATH_ESGETMAPPING = "/esgetmapping"

	PATH_DBQUERY    = "/dbquery"
	PATH_DBMETADATA = "/dbmetadata"
)
