package file

func Register() {
	NewUploadFuncService().Register()
	NewDownloadFuncService().Register()
	NewInt64FuncService().Register()
}
