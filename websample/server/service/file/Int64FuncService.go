package file

import (
	"fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
)

type Int64FuncService struct {
	funchandler.FuncService
}

func NewInt64FuncService() *Int64FuncService {
	return new(Int64FuncService).init()
}

func (this *Int64FuncService) init() *Int64FuncService {
	this.WebBasePath = "file"

	this.InitRoute(this, http.MethodPost, "int64")
	return this
}

// @Summary		文件服务
// @Description	文件服务
// @Tags			文件服务
// @Produce		json
// @Accept		json
// @Success		200	{object}	basedto.IchubResult	"成功"
// @Failure		400	{object}	string				"请求错误"
// @Failure		500	{object}	string				"内部错误"
// @Router			/file/int64 [post]
func (this *Int64FuncService) Execute(ctx *gin.Context) {
	result := basedto.NewIchubResult()
	result.Data = int64val()
	logrus.Info("data=", result.ToPrettyString())
	ctx.IndentedJSON(http.StatusOK, result)
	//ctx.Data(http.StatusOK, "application/json", result.ToJsonBytes())
}

type Int64Stru struct {
	I int64
}

func NewInt64Stru(i int64) *Int64Stru {
	return &Int64Stru{I: i}
}

func int64val() *Int64Stru {

	//	var webcli = f.FindWebClientEngine()
	//	var result = webcli.GetShortUrl("")
	//	logrus.Info(result)
	var i = baseutils.SnowflakeNextVal()
	fmt.Println(i)

	var is = NewInt64Stru(i)
	logrus.Info(jsonutils.ToJsonPretty(is))
	var ss = jsonutils.ToJsonPretty(is)
	var nn = NewInt64Stru(0)
	jsonutils.FromJson(ss, nn)
	fmt.Println(nn)
	return nn
}
