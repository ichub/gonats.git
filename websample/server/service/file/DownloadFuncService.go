package file

import (
	"fmt"
	"gitee.com/ichub/goconfig/common/base/baseutils/fileutils"
	"gitee.com/ichub/goweb/common/webserver/funchandler"

	//"git.ichub.com/general/esserver/esserver/engineconst"
	"github.com/gin-gonic/gin"
	"net/http"
)

/*
@Title    文件名称: DownloadFuncService.go
@Description  描述: DownloadFuncService

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
type DownloadFuncService struct {
	funchandler.FuncService
}

func NewDownloadFuncService() *DownloadFuncService {
	return new(DownloadFuncService).init()
}

func (this *DownloadFuncService) init() *DownloadFuncService {
	this.WebBasePath = "file"
	this.InitRoute(this, http.MethodPost, "/download")
	return this
}

//	@Summary		命令
//	@Tags			文件服务
//	@Description	download命令
//
// ##// @Param filename query string true "file name"
//
//	@Success		200	{object}	gin.Context
//	@Router			/file/download [post]
func (this *DownloadFuncService) Execute(c *gin.Context) {
	//	this.FuncService.Execute(c)
	this.downloadFile(c)
	//	c.JSON(200, basedto.ResultSuccessData("下载成功"))

}

func (this *DownloadFuncService) downloadFile(ctx *gin.Context) {
	filename := ctx.DefaultQuery("filename", "leijm.yaml")
	//fmt.Sprintf("attachment; filename=%s", filename)对下载的文件重命名
	ctx.Writer.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=%s", filename))
	ctx.Writer.Header().Add("Content-Type", "application/octet-stream")

	var root = fileutils.FindRootDir()
	ctx.File(root + "/config/ichub.yml")

}
