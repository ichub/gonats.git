package file

import (
	"fmt"
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
	"github.com/gin-gonic/gin"
	"net/http"
)

/*
@Title    文件名称: DownloadFuncService.go
@Description  描述: DownloadFuncService

@Author  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
@Update  作者: leijianming@163.com  时间(2024-02-18 22:38:21)
*/
type UploadFuncService struct {
	funchandler.FuncService
}

func NewUploadFuncService() *UploadFuncService {
	return new(UploadFuncService).init()
}

func (this *UploadFuncService) init() *UploadFuncService {
	this.WebBasePath = "file"

	this.InitRoute(this, http.MethodPost, "upload")
	return this
}

// @Summary		命令
// @Tags			文件服务
// @Description	命令
// @Produce		json
// @Accept		multipart/form-data
// @Param		file	formData	file	true	"file"
// @Success		200		{object}	basedto.IchubResult		"成功"
// @Failure		400		{object}	string	"请求错误"
// @Failure		500		{object}	string	"内部错误"
// @Router			/file/upload [post]
func (this *UploadFuncService) Execute(c *gin.Context) {
	//	this.FuncService.Execute(c)
	this.uploadFile(c)
	c.JSON(200, basedto.ResultSuccessData("上传成功"))

}

func (this *UploadFuncService) ExeFunc(ctx *gin.Context) (any, error) {
	return nil, nil

}
func (this *UploadFuncService) uploadFile(c *gin.Context) {
	// 限制文件大小为5MB
	c.Request.ParseMultipartForm(5 << 20)

	// 获取文件
	file, err := c.FormFile("file")
	if err != nil {
		// 如果有错误，返回错误信息
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		fmt.Println("fn=", file)

		return
	}

	// 指定文件应保存的路径
	filePath := "./logs/" + file.Filename

	// 将文件保存到指定路径
	err = c.SaveUploadedFile(file, filePath)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
}
