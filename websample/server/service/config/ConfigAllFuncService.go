package config

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goweb/common/webclient"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
	"github.com/gin-gonic/gin"
	"net/http"
)

type ConfigAllFuncService struct {
	funchandler.FuncService
}

func NewConfigAllFuncService() *ConfigAllFuncService {
	return new(ConfigAllFuncService).init()
}

func (this *ConfigAllFuncService) init() *ConfigAllFuncService {
	this.WebBasePath = "config"
	this.InitRoute(this, http.MethodGet, "/all")
	return this
}

// @Summary		配置服务
// @Tags			配置服务
// @Description	配置服务
// @Produce		json
// @Success		200	{object}	basedto.IchubResult	"成功"
// @Failure		400	{object}	string	"请求错误"
// @Failure		500	{object}	string	"内部错误"
// @Router			/config/all [get]
func (this *ConfigAllFuncService) Execute(c *gin.Context) {
	var webclient = webclient.Default()
	webclient.Config.Read()

	var result = basedto.ResultSuccessData(webclient.Config)
	c.IndentedJSON(200, result)

}
