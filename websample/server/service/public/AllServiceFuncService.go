package public

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/goweb/common/webclient"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
	"gitee.com/ichub/gonats/websample/server/webcommon"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
)

type AllServiceFuncService struct {
	funchandler.FuncService
}

func NewAllServiceFuncService() *AllServiceFuncService {
	return new(AllServiceFuncService).init()
}

func (this *AllServiceFuncService) init() *AllServiceFuncService {
	this.WebBasePath = "public"

	this.InitRoute(this, http.MethodGet, webcommon.PATH_AllService)

	return this
}

// @Summary		根据服务名找服务
// @Tags			公共服务
// @Description	根据服务名找服务
// @Produce		json
// @Param			serverName	query		string	true	"query params: string"
// @Success		200			{object}	string	"成功"
// @Failure		400			{object}	basedto.IchubResult		"请求错误"
// @Failure		500			{object}	string	"内部错误"
// @Router			/public/allservice [get]
func (this *AllServiceFuncService) Execute(c *gin.Context) {
	var webclient = webclient.Default()
	var srvName = c.Query("serverName")
	serviceList := webclient.GetServiceAll(srvName)

	var result = basedto.NewIchubResult().SuccessData(serviceList)
	logrus.Info("all=", jsonutils.ToJsonPretty(result))
	c.IndentedJSON(200, result)

}
