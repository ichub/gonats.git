package public

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goconfig/common/base/baseutils/jsonutils"
	"gitee.com/ichub/goweb/common/webclient"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
	"gitee.com/ichub/gonats/websample/server/webcommon"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
)

type AllAddrFuncService struct {
	funchandler.FuncService
}

func NewAllAddrFuncService() *AllAddrFuncService {
	return new(AllAddrFuncService).init()
}

func (this *AllAddrFuncService) init() *AllAddrFuncService {
	this.WebBasePath = "public"

	this.InitRoute(this, http.MethodGet, webcommon.PATH_AllAddr)

	return this
}

// @Summary		根据服务名找服务
// @Tags			公共服务
// @Description	根据服务名找服务
// @Produce		json
// @Success		200	{object}	basedto.IchubResult		"成功"
// @Failure		400	{object}	string	"请求错误"
// @Failure		500	{object}	string	"内部错误"
// @Router			/public/alladdr [get]
func (this *AllAddrFuncService) Execute(c *gin.Context) {
	var webclient = webclient.Default()
	serviceList := webclient.GetServiceAddrs()
	logrus.Println("all=", jsonutils.ToJsonPretty(serviceList))
	var result = basedto.NewIchubResult().SuccessData(serviceList)
	logrus.Println("all=", jsonutils.ToJsonPretty(result))
	c.IndentedJSON(200, result)

}
