package public

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
	"gitee.com/ichub/gonats/websample/server/webcommon"
	"github.com/gin-gonic/gin"
	"net/http"
)

type HelloWorldFuncService struct {
	funchandler.FuncService
}

func NewHelloWorldFuncService() *HelloWorldFuncService {
	return new(HelloWorldFuncService).init()
}

func (this *HelloWorldFuncService) init() *HelloWorldFuncService {

	this.InitRoute(this, http.MethodGet, webcommon.PATH_HelloWorld)

	return this
}

// @Summary	你好世界
// @Tags		公共服务
// @Produce	json
// @Success	200	{object}	basedto.IchubResult		"成功"
// @Failure	400	{object}	string	"请求错误"
// @Failure	500	{object}	string	"内部错误"
// @Router		/ [get]
func (this *HelloWorldFuncService) Execute(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, (&basedto.IchubResult{}).SuccessData("Hello World"))

}
