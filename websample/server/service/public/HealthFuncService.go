package public

import (
	"gitee.com/ichub/goconfig/common/base/basedto"
	"gitee.com/ichub/goweb/common/webserver/funchandler"
	"github.com/gin-gonic/gin"
	"net/http"
)

type HealthFuncService struct {
	funchandler.FuncService
}

func NewHealthFuncService() *HealthFuncService {
	return new(HealthFuncService).init()
}

func (this *HealthFuncService) init() *HealthFuncService {
	this.WebBasePath = "public"

	this.InitRoute(this, http.MethodGet, "/health")
	return this
}

// @Summary	健康检查
// @Tags		公共服务
// @Produce	json
// @Success	200	{object}	basedto.IchubResult		"成功"
// @Failure	400	{object}	string						"请求错误"
// @Failure	500	{object}	string						"内部错误"
// @Router		/health [get]
func (this *HealthFuncService) Execute(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, (&basedto.IchubResult{}).SuccessData("ok"))

}
