package nats

import (
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/gonats/gomini/natsserver/async/serverasync"
	"gitee.com/ichub/gonats/gomini/natsserver/sync/serversync"
)

func Start() {
	defer func() {
		if r := recover(); r != nil {
			goutils.Error("[main pfserver] Recovered in ", r)

		}
	}()
	goutils.Info("now starting serverNats....")
	//async Server
	var serverAsync = serverasync.FindBeanServerAsync()
	conn, err := serverAsync.StartServer()
	if err != nil || conn == nil {
		panic("start serverAsync error!")
	}
	defer conn.Close()

	//sync Server同步
	if err := serversync.FindBeanServerSync().StartServer(); err != nil {
		panic("start serverSync error!")
	}

}
