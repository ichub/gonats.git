package gocmd

import (
	"fmt"
	"gitee.com/ichub/goconfig/cmd/goconfig/gomenu"
	"gitee.com/ichub/goconfig/common/base/baseutils/goutils"
	"gitee.com/ichub/goconfig/common/ichubconfig"
	pageesdto "gitee.com/ichub/gomini/mini/general/query/pagees"
	"gitee.com/ichub/gonats/cmd/gonats/nats"
	"gitee.com/ichub/gonats/gomini/gonats/goconsts"
	"gitee.com/ichub/gonats/gomini/gonats/gomsg"
	"gitee.com/ichub/gonats/gomini/natsserver/sync/clisync"
	"github.com/spf13/cobra"
)

func makeReq() *pageesdto.PageesRequest {
	var esRequest = pageesdto.New()
	esRequest.PageSize = 3
	esRequest.IndexName = "ichub_sys_dept"
	esRequest.Source = "dept_id,dept_name"
	return esRequest
}

// vTopicGeneralSyncSTAT
func request() {
	var cli = clisync.FindBeanCliSync()

	var msg = gomsg.FindBeanGonatsMsg()
	msg.Body = makeReq()
	msg.Domain = goconsts.DomainCms
	msg.Topic = goconsts.Topic_DOMAIN_GENEAL_SyncES_CMS
	msg.Action = goconsts.ES_ACTION_QUERY

	var result, err = cli.Request(msg.Encode())
	if err != nil {
		goutils.Error(err)
		return
	}
	var resp = gomsg.NewResp(result)
	resp.Decode()

}
func stat() {
	var cli = clisync.FindBeanCliSync()

	var msg = gomsg.FindBeanGonatsMsg()
	msg.Body = makeReq()
	msg.Domain = goconsts.DomainGeneral
	msg.Topic = goconsts.TopicGeneralSyncSTAT
	msg.Action = goconsts.ES_ACTION_STAT

	var result, err = cli.Request(msg.Encode())
	if err != nil {
		goutils.Error(err)
		return
	}
	var resp = gomsg.NewResp(result)
	resp.Decode()
	goutils.Info(resp.Data)

}

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "config of prj",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("config")
		var cfg = ichubconfig.FindBeanIchubConfig()
		cfg.Read()
		fmt.Println(cfg)
	},
}
var statCmd = &cobra.Command{
	Use:   "stat",
	Short: "stat性能统计",
	Run: func(cmd *cobra.Command, args []string) {

		fmt.Println("nats-性能统计")
		stat()
	},
}
var natsCmd = &cobra.Command{
	Use:   "server",
	Short: "nats server",
	Run: func(cmd *cobra.Command, args []string) {
		nats.Start()
		fmt.Println("nats-server")
	},
}

const pkgname = "gitee.com/ichub/gonats/cmd/gonats/nats"

func init() {

	var mf = gomenu.FindBeanGomenuFactroy()
	mf.SetVersion("1.0")
	mf.SetCmdName("gonats")
	mf.SetBasePkg(pkgname)
	mf.Init()
	mf.AddCommands(statCmd, configCmd, natsCmd)

}

func Execute() {
	gomenu.FindBeanGomenuFactroy().Execute()
}
